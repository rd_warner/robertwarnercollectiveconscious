﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Xml.Serialization;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;


namespace LearningXNATutorial.Model_Objects
{
    public class Image
    {

        public float Alpha;
        public string Text, FontName, ImagePath;
        public Texture2D Texture;
        public Vector2 Position, Scale;
        public Rectangle SourceRect;
        public string Effects;
        public bool IsActive;


        private Vector2 origin;
        private ContentManager content;
        private RenderTarget2D renderTarget;
        private SpriteFont font;
        private const float ROTATION_VALUE = 0.0f;
        private const float LAYER_DEPTH = 0.0f;
        private Dictionary<string, ImageEffect> effectCollection;

        public FadeEffect FadeEffect;

        private void SetEffect<T>(ref T effect)
        {
            if (effect == null)
            {
                effect = (T)Activator.CreateInstance(typeof(T));
            }
            else
            {
                (effect as ImageEffect).IsActive = true;
                var obj = this;
                (effect as ImageEffect).LoadContent(ref obj);
            }

            effectCollection.Add(effect.GetType().ToString().Replace("LearningXNATutorial.Model_Objects.", ""), (effect as ImageEffect));

        }

        public void ActivateEffect(string effect)
        {
            if(effectCollection.ContainsKey(effect))
            {
                effectCollection[effect].IsActive = true;
                var obj = this;
                effectCollection[effect].LoadContent(ref obj);
            }
        }

        public void DeactivateEffect(string effect)
        {
            if(effectCollection.ContainsKey(effect))
            {
                effectCollection[effect].IsActive = false;
                effectCollection[effect].UnloadContent();
            }          
        }

        public Image()
        {
            /*These are all defaults; because the constructor gets called first.
             *   Then, all public variables that have a name that matches something in Xml,
             *   or that are preceeded by an XmlElement-tag get their values overriden.
            */
            ImagePath = String.Empty;
            Text = String.Empty;
            Effects = String.Empty;
            FontName = "Fonts/Orbitron";
            Position = Vector2.Zero;
            Scale = Vector2.One;
            Alpha = 1.0f;
            SourceRect = Rectangle.Empty;
            effectCollection = new Dictionary<string, ImageEffect>();
        }

        public void LoadContent()
        {
            content =new ContentManager(
                ScreenManager.Instance.ContentLocal.ServiceProvider, "Content");

            if (ImagePath != String.Empty)
            {
                Texture = content.Load<Texture2D>(ImagePath);
            }

            font = content.Load<SpriteFont>(FontName);

            Vector2 dimensions = Vector2.Zero;

            if (Texture != null)
            {
                dimensions.X += Texture.Width;
                dimensions.X += font.MeasureString(Text).X;
                dimensions.Y = Math.Max(Texture.Height, font.MeasureString(Text).Y);
            }
            else
            {
                dimensions.Y = font.MeasureString(Text).Y;
            }

            if (SourceRect.Equals(Rectangle.Empty))
            {
                SourceRect = new Rectangle(0,0, (int)dimensions.X, (int)dimensions.Y);
            }

            renderTarget = new RenderTarget2D(ScreenManager.Instance.GraphicsDevice,
                (int) dimensions.X, (int) dimensions.Y);
            ScreenManager.Instance.GraphicsDevice.SetRenderTarget(renderTarget);
            
            ScreenManager.Instance.GraphicsDevice.Clear(Color.Transparent);
            ScreenManager.Instance.SpriteBatch.Begin();
           
            if (Texture != null)
            {
                ScreenManager.Instance.SpriteBatch.Draw(Texture,Vector2.Zero,Color.White);
            }
            ScreenManager.Instance.SpriteBatch.DrawString(font, Text, Vector2.Zero, Color.White);

            ScreenManager.Instance.SpriteBatch.End();

            Texture = renderTarget;

            ScreenManager.Instance.GraphicsDevice.SetRenderTarget(null);

            SetEffect<FadeEffect>(ref FadeEffect);
            if (Effects != String.Empty)
            {
                string[] stringSplit = Effects.Split(':');
                foreach(string item in stringSplit)
                {
                    ActivateEffect(item);
                }
            }
           
        }


        public void UnloadContent()
        {
            content.Unload();

            foreach (var effect in effectCollection)
            {
                DeactivateEffect(effect.Key);
            }
        }


        public void Update(GameTime gameTimeParam)
        {
            foreach (var effect in effectCollection)
            {
                if (effect.Value.IsActive)
                {
                    effect.Value.Update(gameTimeParam);
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            origin = new Vector2(SourceRect.Width / 2, SourceRect.Height / 2);
            spriteBatch.Draw(Texture, Position+origin, SourceRect, Color.White*Alpha,
                ROTATION_VALUE, origin, Scale, SpriteEffects.None, LAYER_DEPTH);
        }


    }
}
