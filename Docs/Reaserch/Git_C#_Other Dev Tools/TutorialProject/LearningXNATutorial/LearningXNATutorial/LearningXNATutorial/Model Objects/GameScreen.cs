﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace LearningXNATutorial.Model_Objects
{
    /*
     * This is the base class for every other screen out there.
     * Polymorphism.
     * Ergo:  Virtual Methods.
     */
    public class GameScreen
    {

        protected ContentManager content;
        
        [XmlIgnore]
        public Type Type { get; set; }
        

        public string XmlPath;


        public GameScreen ()
        {
            Type = this.GetType();

            XmlPath = "Load/" + Type.ToString().Replace("LearningXNATutorial.Model_Objects", "") + ".xml";
        }


        public virtual void LoadContent()
        {
            content = new ContentManager(
                ScreenManager.Instance.ContentLocal.ServiceProvider, "Content");
        }

        /*
         * Whenever we load in something, we want to unload it when we're done with it.
         */
        public virtual void UnloadContent()
        {
            content.Unload();
        }

        public virtual void Update(GameTime gameTimeParam)
        {
            
        }

        public virtual void Draw(SpriteBatch spriteBatchParam)
        {
            
        }




    }
}
