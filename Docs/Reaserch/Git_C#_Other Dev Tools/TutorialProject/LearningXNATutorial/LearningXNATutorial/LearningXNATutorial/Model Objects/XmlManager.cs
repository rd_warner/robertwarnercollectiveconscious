﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace LearningXNATutorial.Model_Objects
{
    public class XmlManager<T>
    {

        /*
         * Some XML Nots:
         * 
         * So there are some useful notes about xml:
         * [XmlElement ("NAME")] <--- this tag is fucking leet.
         *     it ties whatever immidiately follows it, with whatever has the "NAME"
         *     in the XML file.
         * [XmlIgnore] -- this shit makes the xml serializer ignore whatever immidiately follows it.
         * 
         * For Lists: keep in mind that multiple bitches who are named the same thing in the Xml file
         *      you can load them into a List<Type> LISTNAME by tieing the elements from the xml file with
         *      by using the [XmlElement("ELEMENTNAME(s)")] tag.
         *      
         * Order for var assignment = Constructor, do w/e is in Constructor.
         * Then do the xml serialization etc. etc.
         * then any other methodss.
         * 
         */

        
        public Type Type { get; set; }


        public XmlManager()
        {
            Type = typeof (T);
        } 

        public T Load(string pathParam)
        {
            T instance;
            using (TextReader reader = new StreamReader(pathParam))
            {
                
                XmlSerializer xmlSerializer = new XmlSerializer(Type);
                instance = (T)xmlSerializer.Deserialize(reader);
            }

            return instance;
        }

        public void Save(string pathParam, object objParam)
        {
            using (TextWriter writer = new StreamWriter(pathParam))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(Type);
                xmlSerializer.Serialize(writer,objParam);
            }
        }
    }
}
