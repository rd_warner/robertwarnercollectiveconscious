﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LearningXNATutorial.Model_Objects
{
   
    public class SplashScreen : GameScreen
    {
        public Image Image;
        
        public override void LoadContent()
        {
            base.LoadContent();
            Image.LoadContent();
        }


        public override void UnloadContent()
        {
            base.UnloadContent();
            Image.UnloadContent();
        }


        public override void Update(GameTime gameTimeParam)
        {
            base.Update(gameTimeParam);
            Image.Update(gameTimeParam);
        }


        public override void Draw(SpriteBatch spriteBatchParam)
        {
            Image.Draw(spriteBatchParam);
        }



    }
}
