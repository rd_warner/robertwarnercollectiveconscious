﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LearningXNATutorial.Model_Objects
{

    public class ScreenManager
    {
        private static ScreenManager instance;
        private readonly int SCREENWIDTH = 640;
        private readonly int SCREENHEIGHT = 480;

        XmlManager<GameScreen> xmlGameScreenManager;

        GameScreen currentScreen, nextScreen;

        [XmlIgnore]
        public GraphicsDevice GraphicsDevice;
        [XmlIgnore]
        public SpriteBatch SpriteBatch;
        public Image Image;
        
        [XmlIgnore]
        public bool IsTransitioning { get; private set; }

        [XmlIgnore]
        public ContentManager ContentLocal { get; private set; }
        [XmlIgnore]
        public Vector2 Dimensions { get; private set; }

        public static ScreenManager Instance
        {
            get
            {
                if (instance == null)
                {
                    XmlManager<ScreenManager> xml = new XmlManager<ScreenManager>();
                    instance = xml.Load("Load/ScreenManager.xml");
                }

                return instance;
            }

        }


        public ScreenManager()
        {
            Dimensions = new Vector2(SCREENWIDTH,SCREENHEIGHT);
            currentScreen = new SplashScreen();
            xmlGameScreenManager = new XmlManager<GameScreen>();
            xmlGameScreenManager.Type = currentScreen.Type;
            currentScreen = xmlGameScreenManager.Load("Load/SplashScreen.xml");
        }




        public void ChangeScreens(string screenNameParam)
        {
            nextScreen = (GameScreen)Activator.CreateInstance(Type.GetType("LearningXNATutorial.Model_Objects." + screenNameParam));
            Image.IsActive = true;
            Image.FadeEffect.Increase = true;
            Image.Alpha = 0.0f;
            IsTransitioning = true;
        }

        private void Transition(GameTime gameTimeParam)
        {
            if (IsTransitioning)
            {
                Image.Update(gameTimeParam);
                if (Image.Alpha == 1.0f)
                {
                    currentScreen.UnloadContent();
                    currentScreen = nextScreen;
                    xmlGameScreenManager.Type = currentScreen.Type;

                    if (File.Exists(currentScreen.XmlPath))
                    {
                        currentScreen = xmlGameScreenManager.Load(currentScreen.XmlPath);
                    }
                    currentScreen.LoadContent();
                }
                else if (Image.Alpha == 0.0f)
                {
                    Image.IsActive = false;
                    IsTransitioning = false;
                }
            }
        }




        public void LoadContent(ContentManager contentParam)
        {
            this.ContentLocal = new ContentManager(contentParam.ServiceProvider, "Content");
            currentScreen.LoadContent();
            Image.LoadContent();
        }

        public void UnloadContent()
        {
            currentScreen.UnloadContent();
            Image.UnloadContent();
        }

        public void Update(GameTime gameTimeParam)
        {
            currentScreen.Update(gameTimeParam);
            Transition(gameTimeParam);

            if (Keyboard.GetState().IsKeyDown(Keys.Enter) && !ScreenManager.instance.IsTransitioning)
            {
                ScreenManager.instance.ChangeScreens("SplashScreen");
            }
        }

        public void Draw(SpriteBatch spriteBatchParam)
        {
            currentScreen.Draw(spriteBatchParam);
        }

    }
}
