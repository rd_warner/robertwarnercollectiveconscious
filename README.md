*******This project source control is being transferred over to github -- Link to follow*****
**
# Collective Conscious README #

Application is still in early dev stages but the solution project compiles and runs.
Control flows through the CollectiveConsciousDriver object through a custom game component that
implements a state engine (current idea for implementation of application update functionality). 



### What is this repository for? ###

* Capstone Project 
* .9 Development version; pre-Alpha.

### How do I get set up? ###

* Application requires several third party libraries, including Emotive SDK (for the eeg interface) as well as DigitalRune (for UI)

### Who do I talk to? ###

* rdwarnerop@gmail.com Main developer and conceptual lead of the project.