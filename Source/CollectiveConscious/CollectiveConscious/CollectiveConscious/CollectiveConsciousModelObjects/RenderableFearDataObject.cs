﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CollectiveConscious.CollectiveConsciousFramework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace CollectiveConscious.CollectiveConsciousModelObjects
{
    public class RenderableFearDataObject
    {

        public VertexPositionColor[] PointList { get; private set; }
        public short[] LineListIndicies { get; private set; }

        //Constructor
        public RenderableFearDataObject(List<float> fearDataParam, int renderIndexParam, int totalRenderCountParam, bool newData)
        {
            int xIndex = (newData)? 230 : 435;
            int n = fearDataParam.Count();
            //GeneratePoints generates a point graph from fear data.
            PointList = new VertexPositionColor[n];
            for (int i = 0; i < n; i++)
                PointList[i] = new VertexPositionColor()
                {
                    Position =
                        new Vector3(i*2 + xIndex, (-(fearDataParam[i] * 500) / totalRenderCountParam) + (500 / (totalRenderCountParam) * renderIndexParam) + 120, 0),
                    Color = Color.White
                };

            //links the points into a list
            LineListIndicies = new short[(n * 2) - 2];
            for (int i = 0; i < n - 1; i++)
            {
                LineListIndicies[i * 2] = (short)(i);
                LineListIndicies[(i * 2) + 1] = (short)(i + 1);
            }
        }



    }
}
