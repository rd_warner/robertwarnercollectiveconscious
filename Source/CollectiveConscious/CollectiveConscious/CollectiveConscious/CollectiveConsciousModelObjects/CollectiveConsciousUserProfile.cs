﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CollectiveConscious.CollectiveConsciousFramework;


namespace CollectiveConscious.CollectiveConsciousModelObjects
{
    public class CollectiveConsciousUserProfile
    {
        //Empty public constructor to allow serialization.
        public CollectiveConsciousUserProfile(){}
      
        //--------------------------------------------------------------
        #region Instance Members
        //--------------------------------------------------------------

        public byte[] eegProfileData { get; set; }
        [XmlArray("FearDataHistoryList"),XmlArrayItem(typeof(FearData),ElementName="FearData")]
        public List<FearData> FearDataHistory;
        public string ProfName { get; set; }
        #endregion

        //--------------------------------------------------------------
        #region User Profile Methods
        //--------------------------------------------------------------
        public void RecordFearData(FearData fearDataParam)
        {
            FearDataHistory.Add(fearDataParam);
        }

        #endregion


    }
}
