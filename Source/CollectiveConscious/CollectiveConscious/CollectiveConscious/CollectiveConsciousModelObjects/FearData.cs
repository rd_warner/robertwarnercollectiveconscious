﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using CollectiveConscious.CollectiveConsciousFramework;

namespace CollectiveConscious.CollectiveConsciousModelObjects
{
    public class FearData
    {
        //Empty public constructor to allow serialization
        public FearData() { }

        public DateTime Date { get; set; }

        [XmlArray("EEGBrainWaveValuesList"), XmlArrayItem(typeof(float), ElementName = "BrainWaveValue")]
        public List<float> EEGBrainWaveValues { get; set; }
        
    }
}
