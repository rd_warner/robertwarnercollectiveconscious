﻿using DigitalRune.Game.UI;
using DigitalRune.Game.UI.Controls;
using DigitalRune.Graphics;
using DigitalRune.Mathematics.Algebra;
using DigitalRune.ServiceLocation;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollectiveConscious.CollectiveConsciousModelObjects;
using CollectiveConscious.FDVEs;
using CollectiveConscious.Vendor;

namespace CollectiveConscious.CollectiveConsciousFramework
{

    public class CollectiveConsciousGuiController
    {
        //--------------------------------------------------------------
        #region Instance Members
        //--------------------------------------------------------------
        private static CollectiveConsciousGuiController _instance;
        private readonly ServiceContainer _services;
        private readonly IGraphicsService _graphicsService;
        private readonly Game _game;
        public SpriteBatch _spriteBatch;
        public SpriteFont _spriteFont;
        public GuiGraphicsScreen _SecondaryUIScreen { get; private set; }
        private DelegateGraphicsScreen _mainScreen;
        public DelegateGraphicsScreen MainScreen
        {
            get
            {
                return _mainScreen;
            }
            set
            {
                _mainScreen = value;
                value.RenderCallback = DefaultScreenRenderScreen;
            }
        }

        // Menu-Window Specific Values
        private const int MENUWINDOW_WIDTH = 200;
        private const int MENUWINDOW_HEIGHT = 520;
        private const int MENUBUTTON_WIDTH = 165;
        private const int MENUBUTTON_HEIGHT = 35;
        private const int MENUBUTTON_MARGIN = 12;
        private const int FIRSTMENUBUTTON_MARGIN = 10;
        private Button userHistoryButton;
        private Button FDVEButton;
        private Button FRVEButton;
        private Button brainWaveInterafaceSettingsButton;
        private Button vRInterafaceSettingsButton;
        private Button exitButton;

        // Update-Window Specific Values
        private const int UPDATEWINDOW_POSITION_X = 200;
        private const int UPDATEWINDOW_POSITION_Y = 100;
        private const int UPDATEWINDOW_WIDTH = 800;
        private const int UPDATEWINDOW_HEIGHT = 554;

        // Log-In-Window Specific Values
        private readonly string UserProfileDirectoryPath = "UserProfileData/";
        private const int LOGINWINDOW_POSITION_X = 230;
        private const int LOGINWINDOW_POSITION_Y = 100;
        private const int LOGINWINDOW_WIDTH = 400;
        private const int LOGINWINDOW_HEIGHT = 300;

        //Error-Msg Window
        private const int ERRORWINDOW_POSITION_X = 600;
        private const int ERRORWINDOW_POSITION_Y = 300;



        //Default Screen Instance Members Screen Dimensions and Subscreen Render Values
        private readonly Color COLOR_TRANSPARENT = Color.White;
        private readonly int APPLICATION_HEIGHT = 720;
        //private readonly int MAINWINDOW_WIDTH = 1080;
        private readonly int SUBSCREEN_WIDTH = 200;
        private readonly float SUBSCREEN_ROTATION = 0f;
        private readonly Vector2 SUBSCREEN_ORIGIN = new Vector2(0, 0);
        private readonly Vector2 SUBSCREEN_POSITION = new Vector2(0, 0);
        private readonly Vector2 SUBSCREEN_SCALE = new Vector2(1, 1);
        private readonly SpriteEffects SUBSCREEN_EFFECTS = Microsoft.Xna.Framework.Graphics.SpriteEffects.None;
        private readonly float SUBSCREEN_DEPTH = 1;

        //UI Element Constant Values and Instance Members
        private const int UHS_BUTTON_WIDTH = 145;
        private const int UHS_BUTTON_HEIGHT = 36;
        private List<FearData> FearDataToBeGraphed;
        public bool Is_FearDataToBeGraphed_Changed { get; set; }
        private RenderableFearDataObject[] RenderableFearData;
        //Fear Data Window
        private const int FEARDATAWINDOW_X = 230;
        private const int FEARDATAWINDOW_Y = 120;
        private const int FEARDATAWINDOW_WIDTH = 200;
        private const int FEARDATAWINDOW_HEIGHT = 500;
        private const int UserProfileInfoUI_X = 690;
        private const int UserProfileInfoUI_Y = 55;
        //Fear Graph Window
        private const int FEARGRAPHWINDOW_X = 430;
        private const int FEARGRAPHWINDOW_Y = 120;
        private const int FEARGRAPHWINDOW_WIDTH = 820;
        private const int FEARGRAPHWINDOW_HEIGHT = 500;


        //Fear Graph Components
        private Matrix worldMatrix;
        private Matrix viewMatrix;
        private Matrix projectionMatrix;
        private BasicEffect basicEffect;
        private VertexPositionColor[] _pointList;
        private short[] lineListIndices;
        //private readonly int FEARGRAPH_MINY = 100;
        private AlphaFDVE OculusDemo;
        public bool CurrentlyRecording { get; private set; }
        public FearData currentRecordingFearData;
        private RenderableFearDataObject activeRecordingFearData;
        Button startRecordButton;
        Button stopRecordButton;

        //default screen specific instance members.
        private Texture2D backgroundTexture;

        #endregion

        //--------------------------------------------------------------
        #region Constructor and Instance Method
        //--------------------------------------------------------------
        private CollectiveConsciousGuiController()
        {
            //standard initializaiton:
            _services = (ServiceContainer)ServiceLocator.Current;
            _graphicsService = _services.GetInstance<IGraphicsService>();
            _game = _services.GetInstance<Game>();

            var UIContentManager = (ContentManager)_services.GetInstance(typeof(ContentManager), "UIContent");
            _spriteBatch = new SpriteBatch(_graphicsService.GraphicsDevice);
            _spriteFont = UIContentManager.Load<SpriteFont>("Default");

            SecondaryUIScreenInstantiation();

        }

        public static CollectiveConsciousGuiController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new CollectiveConsciousGuiController();
                }
                return _instance;
            }
        }

        private void SecondaryUIScreenInstantiation()
        {

            _SecondaryUIScreen = new GuiGraphicsScreen(_services, "SecondaryUIScreen");
            //_SecondaryUIScreen.UIScreen.IsVisible = false;
            _graphicsService.Screens.Add(_SecondaryUIScreen);
        }


        #endregion

        //--------------------------------------------------------------
        #region Window Methods
        //--------------------------------------------------------------

        public Window GetMenuWindow()
        {
            Window _windowToReturn;

            StackPanel _menuButtonStackPanel = new StackPanel
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Margin = new Vector4F(10, 5, 0, 0)
            };

            _windowToReturn = new Window
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
                Width = MENUWINDOW_WIDTH,
                Height = MENUWINDOW_HEIGHT,
                CloseButtonStyle = string.Empty,
                Title = "Menu",
                CanDrag = false,
                Icon = null,
                CanResize = false,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = _menuButtonStackPanel,
            };

            #region Menu Buttons

            userHistoryButton = new Button
            {
                Name = "userHistoryButton",
                Content = new TextBlock { Text = "User History" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, FIRSTMENUBUTTON_MARGIN, 0, 0),
            };
            userHistoryButton.Click += (s, e) =>
            {
                InstantiateUserHistoryScreenUIElements();
                _mainScreen.RenderCallback = UserHistoryRenderScreen;
            };

            brainWaveInterafaceSettingsButton = new Button
            {
                Name = "brainWaveInterafaceSettingsButton",
                Content = new TextBlock { Text = "Brain Wave Interaface Settings" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
                //IsVisible = false,
            };
            brainWaveInterafaceSettingsButton.Click += (s, e) =>
            {
                InstantiateEEGSettingsScreenUIElements();
                _mainScreen.RenderCallback = EEGSettingsRenderScreen;
            };

            vRInterafaceSettingsButton = new Button
            {
                Name = "vRInterafaceSettingsButton",
                Content = new TextBlock { Text = "Virtual Environment Interface Settings" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
                //IsVisible = false,
            };
            vRInterafaceSettingsButton.Click += (s, e) =>
            {
                InstantiateOculusSettingsScreenUIElements();
                _mainScreen.RenderCallback = OculusRenderScreen;
            };

            FDVEButton = new Button
            {
                Name = "FDVEButton",
                Content = new TextBlock { Text = "Diagnose Anxieties" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
            };
            FDVEButton.Click += (s, e) =>
            {
                InstantiateFearGraphScreenUIElements();
                _mainScreen.RenderCallback = FearGraphRenderScreen;
            };

            FRVEButton = new Button
            {
                Name = "FRVEButton",
                Content = new TextBlock { Text = "Fear Reduction Therapy Environment" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
                //IsVisible = false,
            };
            FRVEButton.Click += (s, e) =>
            {
                InstantiateFearGraphScreenUIElements();
                _mainScreen.RenderCallback = FearGraphRenderScreen;
            };

            exitButton = new Button
            {
                Name = "exitButton",
                Content = new TextBlock { Text = "Exit" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
            };
            exitButton.Click += (s, e) =>
            {
                _game.Exit();

            };


            if (!EEGManager.Instance.IsUserLoggedIn())
            {
                HideMenuFunctions();
            }


            #endregion

            _menuButtonStackPanel.Children.Add(userHistoryButton);
            _menuButtonStackPanel.Children.Add(FDVEButton);
            _menuButtonStackPanel.Children.Add(FRVEButton);
            _menuButtonStackPanel.Children.Add(brainWaveInterafaceSettingsButton);
            _menuButtonStackPanel.Children.Add(vRInterafaceSettingsButton);
            _menuButtonStackPanel.Children.Add(exitButton);


            return _windowToReturn;
        }

        public Window GetUpdateWindow()
        {
            Window _windowToReturn;

            _windowToReturn = new Window
            {
                Title = "Collective Conscious Cliet Updater",
                X = UPDATEWINDOW_POSITION_X,
                Y = UPDATEWINDOW_POSITION_Y,
                Width = UPDATEWINDOW_WIDTH,
                Height = UPDATEWINDOW_HEIGHT,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = true,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = new StackPanel
                {
                    Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                },
            };

            return _windowToReturn;
        }

        public Window GetLogInWindow()
        {
            Window _windowToReturn;

            _windowToReturn = new Window
            {
                Name = "LogInWin",
                Title = "User Profile Log In Menu",
                X = LOGINWINDOW_POSITION_X,
                Y = LOGINWINDOW_POSITION_Y,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = true,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                IsVisible = false,
                Content = new StackPanel
                {
                    Orientation = DigitalRune.Game.UI.Orientation.Horizontal,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Vector4F(MENUBUTTON_MARGIN),
                },
            };

            #region Log-In Text Fields

            TextBox newUserProfileNameTextBox = new TextBox
            {
                Name = "newUserProfileNameTextBox",
                Height = MENUBUTTON_HEIGHT,
                Width = MENUBUTTON_WIDTH,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN + MENUBUTTON_MARGIN + MENUBUTTON_HEIGHT, 0, 0),

            };

            #endregion

            #region Log In Window Buttons

            //log in button
            var logInDropButton = new DropDownButton
            {
                Name = "userProfLogInDropButton",
                Content = new TextBlock { Text = "Log In" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, FIRSTMENUBUTTON_MARGIN, 0, 0),
                SelectedIndex = 0,
            };

            //create new profile butotn
            var logInButton = new Button
            {
                Name = "userProfLogInButton",
                Content = new TextBlock { Text = "Log In" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
            };

            //create delete profile butotn
            var deleteProfButton = new Button
            {
                Name = "deleteProfButton",
                Content = new TextBlock { Text = "Delete Selected Profile" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
            };

            //Compiles pre-existing profiles into dropDownButton.
            var fileNames = String.Join(
                   ",",
                   Directory.GetFiles(UserProfileDirectoryPath, "*.xml")
                      .Select(filename =>
                          Path.GetFileNameWithoutExtension(filename)));
            string[] fileNameArray = fileNames.Split(',');
            foreach (string fileName in fileNameArray)
            {
                if (!fileName.Equals(string.Empty))
                {
                    logInDropButton.Items.Add(fileName);
                }
            }

            //Necessary error prevention & user intuitive function
            if (logInDropButton.Items.Count() < 1)
            {
                logInDropButton.IsEnabled = false;
                logInButton.IsEnabled = false;
                deleteProfButton.IsEnabled = false;
            }


            deleteProfButton.Click += (s, e) =>
            {
                string nameParam = logInDropButton.Items[logInDropButton.SelectedIndex].ToString();

                File.Delete(UserProfileDirectoryPath + nameParam + ".xml");

                if (logInDropButton.Items.Count() > 0)
                {
                    logInDropButton.Items.RemoveRange(0, logInDropButton.Items.Count());
                }

                //Compiles pre-existing profiles into dropDownButton.
                var fileNamesPostDelete = String.Join(
                       ",",
                       Directory.GetFiles(UserProfileDirectoryPath, "*.xml")
                          .Select(filename =>
                              Path.GetFileNameWithoutExtension(filename)));
                string[] fileNameArrayPostDelete = fileNamesPostDelete.Split(',');
                foreach (string fileName in fileNameArrayPostDelete)
                {
                    if (!fileName.Equals(string.Empty))
                    {
                        logInDropButton.Items.Add(fileName);
                    }
                }
                logInDropButton.SelectedIndex = 0;
                //Necessary error prevention & user intuitive function
                if (logInDropButton.Items.Count() < 1)
                {
                    logInDropButton.IsEnabled = false;
                    logInButton.IsEnabled = false;
                    deleteProfButton.IsEnabled = false;
                }


            };
            logInButton.Click += (s, e) =>
            {
                string nameParam = logInDropButton.Items[logInDropButton.SelectedIndex].ToString();
                EEGManager.Instance.LogIn(nameParam);
            };

            //create new profile butotn
            var createNewProfile = new Button
            {
                Name = "userProfCreateNewProfileButton",
                Content = new TextBlock { Text = "Create New Profile" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = MENUBUTTON_WIDTH,
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, MENUBUTTON_MARGIN, 0, 0),
            };
            createNewProfile.Click += (s, e) =>
            {
                string nameParam = newUserProfileNameTextBox.Text;
                EEGManager.Instance.CreateNewProfile(nameParam);
            };

            #endregion

            #region Log In Window Structure
            //adding the log-in-window buttons to a vertical stack panel
            StackPanel buttonStackPan = new StackPanel
            {
                Name = "ButtonStackPan",
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Center,
            };

            //adding the log-in-window buttons to a vertical stack panel
            StackPanel textStackPan = new StackPanel
            {
                Name = "TxtStackPan",
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Center,
                Margin = new Vector4F(0, 2, 0, 0),
            };

            textStackPan.Children.Add(logInDropButton);
            textStackPan.Children.Add(newUserProfileNameTextBox);
            buttonStackPan.Children.Add(logInButton);
            buttonStackPan.Children.Add(deleteProfButton);
            buttonStackPan.Children.Add(createNewProfile);

            var logInContent = _windowToReturn.Content as StackPanel;
            logInContent.Children.Add(textStackPan);
            logInContent.Children.Add(buttonStackPan);

            #endregion

            return _windowToReturn;
        }

        public Window GetLogInErrorWindow(string errorMsgParam)
        {
            Window _windowToReturn;


            var tempguiScreen = (GuiGraphicsScreen)Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<IGraphicsService>().Screens[0];
            var tempWindow = (Window)tempguiScreen.UIScreen.Children[1];
            var tempOuterStack = tempWindow.Content as StackPanel;
            var buttonStack = (StackPanel)tempOuterStack.Children[1];
            var txtStack = (StackPanel)tempOuterStack.Children[0];
            foreach (UIControl ui in buttonStack.Children)
            {
                ui.IsEnabled = false;
            }
            foreach (UIControl ui in txtStack.Children)
            {
                ui.IsEnabled = false;
            }

            _windowToReturn = new Window
            {
                Title = "Error",
                X = ERRORWINDOW_POSITION_X,
                Y = ERRORWINDOW_POSITION_Y,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = true,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = new StackPanel
                {
                    Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                },
            };


            #region Error Msg Text Field


            TextBlock errorMsgTxtBlock = new TextBlock()
            {
                Name = "errorMsgTextBlock", // Control names, while optional, really assist in debuging.
                Text = "Error:  " + errorMsgParam,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsVisible = true,
            };



            #endregion

            #region Error Window Buttons
            //confirm Button
            var confirmButton = new Button
            {
                Name = "confirmButton",
                Content = new TextBlock { Text = "OK" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = (MENUBUTTON_WIDTH / 2),
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, FIRSTMENUBUTTON_MARGIN, 0, 0),
            };
            confirmButton.Click += (s, e) =>
            {
                foreach (UIControl ui in buttonStack.Children)
                {
                    ui.IsEnabled = true;
                }
                foreach (UIControl ui in txtStack.Children)
                {
                    ui.IsEnabled = true;
                }
                _windowToReturn.Close();
            };

            #endregion

            var errorWindowContent = _windowToReturn.Content as StackPanel;
            errorWindowContent.Children.Add(errorMsgTxtBlock);
            errorWindowContent.Children.Add(confirmButton);

            return _windowToReturn;
        }

        public void HideMenuFunctions()
        {
            userHistoryButton.IsEnabled = false;
            FDVEButton.IsEnabled = false;
            FRVEButton.IsEnabled = false;
            brainWaveInterafaceSettingsButton.IsEnabled = false;
            vRInterafaceSettingsButton.IsEnabled = false;

            InstantiateDefaultScreenUIElements();
            _mainScreen.RenderCallback = DefaultScreenRenderScreen;
        }

        private void RecordStart()
        {
            userHistoryButton.IsEnabled = false;
            FDVEButton.IsEnabled = false;
            FRVEButton.IsEnabled = false;
            brainWaveInterafaceSettingsButton.IsEnabled = false;
            vRInterafaceSettingsButton.IsEnabled = false;
            exitButton.IsEnabled = false;
            startRecordButton.IsEnabled = false;
            stopRecordButton.IsEnabled = true;
        }

        private void RecordStop()
        {
            userHistoryButton.IsEnabled = true;
            FDVEButton.IsEnabled = true;
            FRVEButton.IsEnabled = true;
            brainWaveInterafaceSettingsButton.IsEnabled = true;
            vRInterafaceSettingsButton.IsEnabled = true;
            exitButton.IsEnabled = true;
            startRecordButton.IsEnabled = true;
        }

        public void UnHideMenuFunctions()
        {
            userHistoryButton.IsEnabled = true;
            FDVEButton.IsEnabled = true;
            FRVEButton.IsEnabled = true;
            brainWaveInterafaceSettingsButton.IsEnabled = true;
            vRInterafaceSettingsButton.IsEnabled = true;
        }

        #endregion

        //--------------------------------------------------------------
        #region Default Screen Render Method and UI Instantiation.
        //--------------------------------------------------------------
        private void DefaultScreenRenderScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;
            backgroundTexture = _game.Content.Load<Texture2D>("Images/Technology_Background");

            _spriteBatch.Begin();

            //default Screen graphic & text.
            _spriteBatch.DrawString(_spriteFont, "Welcome to CollectiveConscious.", new Vector2(645, 25), COLOR_TRANSPARENT);
            _spriteBatch.DrawString(_spriteFont, "An adaptation of psychotherapy from a software engineering perspective.", new Vector2(518, 50), COLOR_TRANSPARENT);
            _spriteBatch.DrawString(_spriteFont, "Robert David Warner", new Vector2(685, 680), Color.White);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(250, 85, 980, 575), COLOR_TRANSPARENT);

            //render the SubScreen:
            _spriteBatch.Draw(context.SourceTexture,
                SUBSCREEN_POSITION,
                new Microsoft.Xna.Framework.Rectangle(0, 0, SUBSCREEN_WIDTH, APPLICATION_HEIGHT),
                COLOR_TRANSPARENT,
                SUBSCREEN_ROTATION,
                SUBSCREEN_ORIGIN,
                SUBSCREEN_SCALE,
                SUBSCREEN_EFFECTS,
                SUBSCREEN_DEPTH);

            _spriteBatch.End();
        }

        //Unique UI Elements for User Default Screen
        private void InstantiateDefaultScreenUIElements()
        {
            //Show the screen if it is not yet visible.
            if (_SecondaryUIScreen.UIScreen.IsVisible)
            {
                _SecondaryUIScreen.UIScreen.IsVisible = false;
            }

            //Clear previous UI Elements form UIScreen (from other MenuOption-Screens)
            _SecondaryUIScreen.UIScreen.Children.Clear();


        }

        #endregion

        //--------------------------------------------------------------
        #region User History Render Method and UI Instantiation.
        //--------------------------------------------------------------
        private void UserHistoryRenderScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;
            backgroundTexture = _game.Content.Load<Texture2D>("Images/Grey_Background");

            _spriteBatch.Begin();

            //Render Background
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(220, 25, 1035, 670), Color.Gray);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(225, 30, 1025, 70), Color.DarkCyan);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(FEARGRAPHWINDOW_X, FEARGRAPHWINDOW_Y, FEARGRAPHWINDOW_WIDTH, FEARGRAPHWINDOW_HEIGHT), Color.Black);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(225, 620, 1025, 70), Color.DarkCyan);

            //Render Title and User-Info TextFields
            _spriteBatch.DrawString(_spriteFont, "User Profile History.", new Vector2(230, 30), Color.White);
            _spriteBatch.DrawString(_spriteFont, "User Name:    ", new Vector2(456, 45), Color.Black);
            _spriteBatch.DrawString(_spriteFont, "Password:     ", new Vector2(456, 65), Color.Black);
            _spriteBatch.DrawString(_spriteFont, EEGManager.Instance.UserProfile.ProfName, new Vector2(570, 45), Color.GreenYellow);
            _spriteBatch.DrawString(_spriteFont, "*********", new Vector2(570, 65), Color.GreenYellow);

            _spriteBatch.DrawString(_spriteFont, "Fear Brain-Wave Graph", new Vector2(640, 650), Color.GreenYellow);

            //render the SubScreen:
            _spriteBatch.Draw(context.SourceTexture,
                SUBSCREEN_POSITION,
                new Microsoft.Xna.Framework.Rectangle(0, 0, SUBSCREEN_WIDTH, APPLICATION_HEIGHT),
                COLOR_TRANSPARENT,
                SUBSCREEN_ROTATION,
                SUBSCREEN_ORIGIN,
                SUBSCREEN_SCALE,
                SUBSCREEN_EFFECTS,
                SUBSCREEN_DEPTH);

            _spriteBatch.End();

            //Render Fear-Graphs
            if (Is_FearDataToBeGraphed_Changed)
            {
                RenderableFearData = TransformFearDataToRenderableForm(FearDataToBeGraphed);
                Is_FearDataToBeGraphed_Changed = false;
            }

            if (RenderableFearData != null)
            {
                _spriteBatch.Begin();
                foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    foreach (RenderableFearDataObject f in RenderableFearData)
                    {

                        graphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                            PrimitiveType.LineList,
                            f.PointList,
                            0,
                            f.PointList.Length,
                            f.LineListIndicies,
                            0,
                            f.PointList.Length - 1
                        );
                    }
                }
                _spriteBatch.End();
            }

        }


        //Unique UI Elements for User History Screen
        private void InstantiateUserHistoryScreenUIElements()
        {

            #region Clear Previous UI Elements & Set Visibility
            //Show the screen if it is not yet visible.
            if (!_SecondaryUIScreen.UIScreen.IsVisible)
            {
                _SecondaryUIScreen.UIScreen.IsVisible = true;
            }

            //Clear previous UI Elements form UIScreen (from other MenuOption-Screens)
            _SecondaryUIScreen.UIScreen.Children.Clear();
            Is_FearDataToBeGraphed_Changed = false;
            FearDataToBeGraphed = new List<FearData>();
            RenderableFearData = null;
            #endregion

            #region Profile Information UI Elements

            //Used for positioning of Profile Information User-Interface elements
            var UserProfileInfoStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
                X = UserProfileInfoUI_X,
                Y = UserProfileInfoUI_Y,
            };

            //Button for updating Profile Information.
            var _UpdateProfileInfoButton = new Button()
            {
                Name = "UpdateProfileInfoButton",
                Content = new TextBlock { Text = "Update User Information" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = UHS_BUTTON_WIDTH,
                Height = UHS_BUTTON_HEIGHT,
                IsVisible = true,
                IsEnabled = true,
            };
            _UpdateProfileInfoButton.Click += (s, e) =>
            {

            };

            UserProfileInfoStackPanel.Children.Add(_UpdateProfileInfoButton);

            #endregion

            #region Fear History Scrollable Window

            //The Content of the Fear History Scroll Viewer
            var FearDataStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
            };

            //The Scrollable wrapper for the Fear History Content
            var FearDataScrollViewer = new ScrollViewer()
            {
                Name = "FearDataScrollViewer",
                HorizontalAlignment = HorizontalAlignment.Center,
                Width = FEARDATAWINDOW_WIDTH - 10,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Auto,

                Content = FearDataStackPanel,
            };

            //Add each Fear data Object belonging to the current UserProfile to the Scrollable Wrapper in the form of a button.
            //When that button is clicked; the Fear Data Object is added to a collection of FearData objects To-Be-Graphed.
            foreach (FearData f in EEGManager.Instance.UserProfile.FearDataHistory)
            {
                var newFearDataElement = new Button()
                {
                    Name = f.ToString(),
                    Width = MENUBUTTON_WIDTH,
                    Height = MENUBUTTON_HEIGHT,
                    Content = new TextBlock { Text = f.Date.Month + ", " + f.Date.Day + ", " + f.Date.Year },
                    FocusWhenMouseOver = true,
                    Focusable = false,
                    IsVisible = true,
                    IsEnabled = true,
                    Opacity = .5f,
                };
                newFearDataElement.Click += (s, e) =>
                {

                    if (FearDataToBeGraphed.Contains(f))
                    {
                        newFearDataElement.Opacity = .5f;
                        FearDataToBeGraphed.Remove(f);
                    }
                    else
                    {
                        newFearDataElement.Opacity = 1f;
                        FearDataToBeGraphed.Add(f);
                    }
                    Is_FearDataToBeGraphed_Changed = true;
                };
                FearDataStackPanel.Children.Add(newFearDataElement);

            }

            var FearDataWindow = new Window()
            {
                Title = "Fear Data History",
                X = FEARDATAWINDOW_X - 5,
                Y = FEARDATAWINDOW_Y,
                Width = FEARDATAWINDOW_WIDTH + 5,
                Height = FEARDATAWINDOW_HEIGHT,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = false,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = FearDataScrollViewer,
            };


            #endregion


            #region Fear History Scrollable Graph

            worldMatrix = Matrix.Identity;
            viewMatrix = Matrix.CreateLookAt(new Vector3(0.0f, 0.0f, 1.0f), Vector3.Zero, Vector3.Up);
            projectionMatrix = Matrix.CreateOrthographicOffCenter(0, (float)_SecondaryUIScreen.GraphicsService.GraphicsDevice.Viewport.Width,
                (float)_SecondaryUIScreen.GraphicsService.GraphicsDevice.Viewport.Height, 0, 1.0f, 1000.0f);

            basicEffect = new BasicEffect(_SecondaryUIScreen.GraphicsService.GraphicsDevice);
            basicEffect.World = worldMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;

            basicEffect.VertexColorEnabled = true; //important for color

            #endregion

            _SecondaryUIScreen.UIScreen.Children.Add(UserProfileInfoStackPanel);
            _SecondaryUIScreen.UIScreen.Children.Add(FearDataWindow);

        }

        #endregion

        //--------------------------------------------------------------
        #region EEG Settings Render Method and UI Instantiation.
        //--------------------------------------------------------------

        //EEG Settings Screen Render Method.
        private void EEGSettingsRenderScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;

            _spriteBatch.Begin();

            _spriteBatch.DrawString(_spriteFont, "EEG Settings", new Vector2(250, 75), Color.White);

            //render the SubScreen:
            _spriteBatch.Draw(context.SourceTexture,
                SUBSCREEN_POSITION,
                new Microsoft.Xna.Framework.Rectangle(0, 0, SUBSCREEN_WIDTH, APPLICATION_HEIGHT),
                COLOR_TRANSPARENT,
                SUBSCREEN_ROTATION,
                SUBSCREEN_ORIGIN,
                SUBSCREEN_SCALE,
                SUBSCREEN_EFFECTS,
                SUBSCREEN_DEPTH);

            _spriteBatch.End();
        }

        //Unique UI Elements for User EEG Settings Screen
        private void InstantiateEEGSettingsScreenUIElements()
        {
            //Show the screen if it is not yet visible.
            if (!_SecondaryUIScreen.UIScreen.IsVisible)
            {
                _SecondaryUIScreen.UIScreen.IsVisible = true;
            }

            //Clear previous UI Elements form UIScreen (from other MenuOption-Screens)
            _SecondaryUIScreen.UIScreen.Children.Clear();


            var UserProfileInfoStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
                X = UserProfileInfoUI_X,
                Y = UserProfileInfoUI_Y,
            };

            var FearDataStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
            };

            var FearDataScrollViewer = new ScrollViewer()
            {
                Name = "FearDataScrollViewer",
                HorizontalAlignment = HorizontalAlignment.Center,
                Width = FEARDATAWINDOW_WIDTH - 10,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Visible,

                Content = FearDataStackPanel,
            };


            var _UpdateProfileInfoButton = new Button()
            {
                Name = "UpdateProfileInfoButton",
                Content = new TextBlock { Text = "Update User Information" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = UHS_BUTTON_WIDTH,
                Height = UHS_BUTTON_HEIGHT,
                IsVisible = true,
                IsEnabled = true,
            };
            _UpdateProfileInfoButton.Click += (s, e) =>
            {

            };

            var FearDataWindow = new Window()
            {
                Title = "Fear Data History",
                X = FEARDATAWINDOW_X - 5,
                Y = FEARDATAWINDOW_Y,
                Width = FEARDATAWINDOW_WIDTH + 5,
                Height = FEARDATAWINDOW_HEIGHT,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = false,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = FearDataScrollViewer,
            };

            foreach (FearData f in EEGManager.Instance.UserProfile.FearDataHistory)
            {
                var newFearDataElement = new Button()
                {
                    Name = f.ToString(),
                    Width = MENUBUTTON_WIDTH,
                    Height = MENUBUTTON_HEIGHT,
                    Content = new TextBlock { Text = f.Date.Day + "," + f.Date.Month + ", " + f.Date.Year },
                    FocusWhenMouseOver = true,
                    Focusable = false,
                    IsVisible = true,
                    IsEnabled = true,

                };
                newFearDataElement.Click += (s, e) =>
                {

                };
                FearDataStackPanel.Children.Add(newFearDataElement);
            }

            UserProfileInfoStackPanel.Children.Add(_UpdateProfileInfoButton);

            _SecondaryUIScreen.UIScreen.Children.Add(UserProfileInfoStackPanel);
            _SecondaryUIScreen.UIScreen.Children.Add(FearDataWindow);

        }


        #endregion

        //--------------------------------------------------------------
        #region Oculus Settings Render Method and UI Instantiation.
        //--------------------------------------------------------------

        //Oculus Screen Render Method
        private void OculusRenderScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;

            _spriteBatch.Begin();

            _spriteBatch.DrawString(_spriteFont, "Oculus Settings Screen", new Vector2(250, 75), Color.White);

            //render the SubScreen:
            _spriteBatch.Draw(context.SourceTexture,
                SUBSCREEN_POSITION,
                new Microsoft.Xna.Framework.Rectangle(0, 0, SUBSCREEN_WIDTH, APPLICATION_HEIGHT),
                COLOR_TRANSPARENT,
                SUBSCREEN_ROTATION,
                SUBSCREEN_ORIGIN,
                SUBSCREEN_SCALE,
                SUBSCREEN_EFFECTS,
                SUBSCREEN_DEPTH);

            _spriteBatch.End();
        }

        //Unique UI Elements for User Oculus Settings Screen
        private void InstantiateOculusSettingsScreenUIElements()
        {
            //Show the screen if it is not yet visible.
            if (!_SecondaryUIScreen.UIScreen.IsVisible)
            {
                _SecondaryUIScreen.UIScreen.IsVisible = true;
            }

            //Clear previous UI Elements form UIScreen (from other MenuOption-Screens)
            _SecondaryUIScreen.UIScreen.Children.Clear();


            var UserProfileInfoStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
                X = UserProfileInfoUI_X,
                Y = UserProfileInfoUI_Y,
            };

            var FearDataStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                Name = "UserProfileInfoStackPanel",
            };

            var FearDataScrollViewer = new ScrollViewer()
            {
                Name = "FearDataScrollViewer",
                HorizontalAlignment = HorizontalAlignment.Center,
                Width = FEARDATAWINDOW_WIDTH - 10,
                HorizontalScrollBarVisibility = ScrollBarVisibility.Auto,
                VerticalScrollBarVisibility = ScrollBarVisibility.Visible,

                Content = FearDataStackPanel,
            };


            var _UpdateProfileInfoButton = new Button()
            {
                Name = "UpdateProfileInfoButton",
                Content = new TextBlock { Text = "Update User Information" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = UHS_BUTTON_WIDTH,
                Height = UHS_BUTTON_HEIGHT,
                IsVisible = true,
                IsEnabled = true,
            };
            _UpdateProfileInfoButton.Click += (s, e) =>
            {

            };

            var FearDataWindow = new Window()
            {
                Title = "Fear Data History",
                X = FEARDATAWINDOW_X - 5,
                Y = FEARDATAWINDOW_Y,
                Width = FEARDATAWINDOW_WIDTH + 5,
                Height = FEARDATAWINDOW_HEIGHT,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = false,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                Content = FearDataScrollViewer,
            };

            foreach (FearData f in EEGManager.Instance.UserProfile.FearDataHistory)
            {
                var newFearDataElement = new Button()
                {
                    Name = f.ToString(),
                    Width = MENUBUTTON_WIDTH,
                    Height = MENUBUTTON_HEIGHT,
                    Content = new TextBlock { Text = f.Date.Day + "," + f.Date.Month + ", " + f.Date.Year },
                    FocusWhenMouseOver = true,
                    Focusable = false,
                    IsVisible = true,
                    IsEnabled = true,

                };
                newFearDataElement.Click += (s, e) =>
                {

                };
                FearDataStackPanel.Children.Add(newFearDataElement);
            }

            UserProfileInfoStackPanel.Children.Add(_UpdateProfileInfoButton);

            _SecondaryUIScreen.UIScreen.Children.Add(UserProfileInfoStackPanel);
            _SecondaryUIScreen.UIScreen.Children.Add(FearDataWindow);

        }


        #endregion

        //--------------------------------------------------------------
        #region FRVE and FDVE Render Method and UI Instantiation.
        //--------------------------------------------------------------

        //FRVE and FDVE Render Method
        private void FearGraphRenderScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;
            backgroundTexture = _game.Content.Load<Texture2D>("Images/Grey_Background");

            _spriteBatch.Begin();

            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(220, 25, 1035, 670), Color.Gray);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(225, 30, 1025, 70), Color.DarkCyan);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(225, 620, 1025, 70), Color.DarkCyan);
            _spriteBatch.Draw(backgroundTexture, new Microsoft.Xna.Framework.Rectangle(225, 100, FEARGRAPHWINDOW_WIDTH + 205, FEARGRAPHWINDOW_HEIGHT + 25), Color.Black);
            _spriteBatch.DrawString(_spriteFont, "Actively Recording Fear Brain-Wave Graph", new Vector2(640, 650), Color.GreenYellow);


            //render the SubScreen:
            _spriteBatch.Draw(context.SourceTexture,
                SUBSCREEN_POSITION,
                new Microsoft.Xna.Framework.Rectangle(0, 0, SUBSCREEN_WIDTH, APPLICATION_HEIGHT),
                COLOR_TRANSPARENT,
                SUBSCREEN_ROTATION,
                SUBSCREEN_ORIGIN,
                SUBSCREEN_SCALE,
                SUBSCREEN_EFFECTS,
                SUBSCREEN_DEPTH);

            _spriteBatch.End();

            if (CurrentlyRecording)
            {
                if (Is_FearDataToBeGraphed_Changed)
                {
                    activeRecordingFearData = new RenderableFearDataObject(currentRecordingFearData.EEGBrainWaveValues, 1, 1, true);
                    Is_FearDataToBeGraphed_Changed = false;
                }
                if (activeRecordingFearData != null)
                {
                    _spriteBatch.Begin();
                    foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
                    {

                        pass.Apply();
                        graphicsDevice.DrawUserIndexedPrimitives<VertexPositionColor>(
                            PrimitiveType.LineList,
                            activeRecordingFearData.PointList,
                            0,
                            activeRecordingFearData.PointList.Length,
                            activeRecordingFearData.LineListIndicies,
                            0,
                            activeRecordingFearData.PointList.Length - 1
                        );

                    }
                    _spriteBatch.End();
                }
            }
        }

        //Unique UI Elements for Fear Graph Screen
        private void InstantiateFearGraphScreenUIElements()
        {
            //Show the screen if it is not yet visible.
            if (!_SecondaryUIScreen.UIScreen.IsVisible)
            {
                _SecondaryUIScreen.UIScreen.IsVisible = true;
            }

            //Clear previous UI Elements form UIScreen (from other MenuOption-Screens)
            _SecondaryUIScreen.UIScreen.Children.Clear();
            Is_FearDataToBeGraphed_Changed = false;

            #region FRVE & FDVE UI Elements

            //Used for positioning of Profile Information User-Interface elements
            var RecordFearStackPanel = new StackPanel()
            {
                Orientation = DigitalRune.Game.UI.Orientation.Horizontal,
                Name = "UserProfileInfoStackPanel",
                X = UserProfileInfoUI_X-100,
                Y = UserProfileInfoUI_Y,
                IsVisible = true,
            };
           
            Window PromptWindow = new Window()
            {
                Title = "Save Fear Data?",
                X = ERRORWINDOW_POSITION_X,
                Y = ERRORWINDOW_POSITION_Y,
                Icon = null,
                CloseButtonStyle = string.Empty,
                CanResize = false,
                CanDrag = true,
                FocusWhenMouseOver = false,
                Focusable = false,
                AutoUnfocus = true,
                IsVisible = false,
                Content = new StackPanel
                {
                    Orientation = DigitalRune.Game.UI.Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                },
            };

            //Button for updating Profile Information.
            stopRecordButton = new Button()
            {
                Name = "StopRecordButton",
                Content = new TextBlock { Text = "Stop Recording" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = UHS_BUTTON_WIDTH,
                Height = UHS_BUTTON_HEIGHT,
                IsVisible = true,
                IsEnabled = false,
            };
            stopRecordButton.Click += (s, e) =>
            {
                stopRecordButton.IsEnabled = false;
                CurrentlyRecording = false;
                //OculusDemo.Dispose();

                //prompt for save-data
                PromptWindow.IsVisible = true;

            };


            #region Save-Prompt Window


            #region Prompt Text Field
            
            TextBlock promptTextBlock = new TextBlock()
            {
                Name = "promptTextBlock", // Control names, while optional, really assist in debuging.
                Text = "Do you wish to save this Fear-Data to your profile?",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsVisible = true,
            };

            #endregion

            #region Prompt Buttons

            var promptButtonStack = new StackPanel()
            {
              
                    Orientation = DigitalRune.Game.UI.Orientation.Horizontal,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
            };

            //confirm Button
            var confirmButton = new Button
            {
                Name = "confirmButton",
                Content = new TextBlock { Text = "SAVE" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = (MENUBUTTON_WIDTH / 2),
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, FIRSTMENUBUTTON_MARGIN, 0, 0),
            };
            confirmButton.Click += (s, e) =>
            {
                EEGManager.Instance.UserProfile.RecordFearData(currentRecordingFearData);
                EEGManager.Instance.UserProfile.eegProfileData = EEGManager.Instance.EEGEngine.GetUserProfile(0).GetBytes();
                EEGManager.Instance.Save();
                currentRecordingFearData = null;

                PromptWindow.IsVisible = false;
                RecordStop();
            };

            //decline Button
            var decline = new Button
            {
                Name = "declineButton",
                Content = new TextBlock { Text = "DON'T SAVE" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = (MENUBUTTON_WIDTH / 2),
                Height = MENUBUTTON_HEIGHT,
                Margin = new Vector4F(0, FIRSTMENUBUTTON_MARGIN, 0, 0),
            };
            decline.Click += (s, e) =>
            {
                currentRecordingFearData = null;
                PromptWindow.IsVisible = false;
                RecordStop();
            };

            promptButtonStack.Children.Add(confirmButton);
            promptButtonStack.Children.Add(decline);

            var promptContent = PromptWindow.Content as StackPanel;
            promptContent.Children.Add(promptTextBlock);
            promptContent.Children.Add(promptButtonStack);


            #endregion
            
            #endregion


            startRecordButton = new Button()
            {
                Name = "StartRecordButton",
                Content = new TextBlock { Text = "Start Recording" },
                FocusWhenMouseOver = true,
                Focusable = false,
                Width = UHS_BUTTON_WIDTH,
                Height = UHS_BUTTON_HEIGHT,
                IsVisible = true,
                IsEnabled = true,
    
            };
            startRecordButton.Click += (s, e) =>
            {
                RecordStart();
                
                currentRecordingFearData = new FearData();
                currentRecordingFearData.Date = DateTime.Now;
                currentRecordingFearData.EEGBrainWaveValues = new List<float>();
                CurrentlyRecording = true;

                OculusDemo = new AlphaFDVE(_game);
                
            };

            RecordFearStackPanel.Children.Add(startRecordButton);
            RecordFearStackPanel.Children.Add(stopRecordButton);

            _SecondaryUIScreen.UIScreen.Children.Add(RecordFearStackPanel);
            _SecondaryUIScreen.UIScreen.Children.Add(PromptWindow);

            #endregion

            #region Render Effect Members

            worldMatrix = Matrix.Identity;
            viewMatrix = Matrix.CreateLookAt(new Vector3(0.0f, 0.0f, 1.0f), Vector3.Zero, Vector3.Up);
            projectionMatrix = Matrix.CreateOrthographicOffCenter(0, (float)_SecondaryUIScreen.GraphicsService.GraphicsDevice.Viewport.Width,
                (float)_SecondaryUIScreen.GraphicsService.GraphicsDevice.Viewport.Height, 0, 0.0f, 1000.0f);
            basicEffect = new BasicEffect(_SecondaryUIScreen.GraphicsService.GraphicsDevice);
            basicEffect.World = worldMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;

            basicEffect.VertexColorEnabled = true; //important for color

            #endregion

        }


        #endregion

        //--------------------------------------------------------------
        #region Additional Universaly Usefull Methods
        //--------------------------------------------------------------
        //Fear Data TransFormation Method
        private RenderableFearDataObject[] TransformFearDataToRenderableForm(List<FearData> fearDataCollectionParam)
        {
            int n = fearDataCollectionParam.Count();
            RenderableFearDataObject[] renderableFearDataCollectionToReturn = new RenderableFearDataObject[n];

            for (int i = 0; i < n; i++)
            {
                renderableFearDataCollectionToReturn[i] = new RenderableFearDataObject(fearDataCollectionParam[i].EEGBrainWaveValues, (i + 1), n, false);
            }

            return renderableFearDataCollectionToReturn;
        }

        #endregion


    }
}