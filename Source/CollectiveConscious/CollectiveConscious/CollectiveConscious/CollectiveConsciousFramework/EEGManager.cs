﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emotiv;
using System.Threading;
using System.IO;
using CollectiveConscious.CollectiveConsciousModelObjects;
using DigitalRune.Graphics;

namespace CollectiveConscious.CollectiveConsciousFramework
{
    public class EEGManager
    {

        //--------------------------------------------------------------
        #region Instance Members
        //--------------------------------------------------------------
        private static EEGManager _instance;
        public EmoEngine EEGEngine = Emotiv.EmoEngine.Instance;
        private Thread emoUpdateThread;

        private const int _userID = 0;
        private const int _emoEnginePollingPeriod = 10000;
        private const int _processTime = 50;
        public EmoState CurrentState { get; private set; }

        private string _profileName = string.Empty;
        public CollectiveConsciousUserProfile UserProfile { get; private set; }

        private readonly string UserProfileDirectoryPath = "UserProfileData/";

        #endregion

        //--------------------------------------------------------------
        #region Constructor and Instance Method
        //--------------------------------------------------------------
        private EEGManager()
        {
            registerDelegates();
            EEGEngine.Connect();
            EEGEngine.ProcessEvents(_emoEnginePollingPeriod);

            emoUpdateThread = new Thread(EngineUpdate);
            emoUpdateThread.Start();

        }

        public static EEGManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EEGManager();
                }
                return _instance;
            }
        }

        #endregion

        //--------------------------------------------------------------
        #region EEG Manager Methods
        //--------------------------------------------------------------
        public bool USBConnected { get; private set; }
        public bool IsUserLoggedIn()
        {
            return !(_profileName.Equals(string.Empty));
        }
        
        public void CreateNewProfile(string nameParam)
        {

            //First check to see that the profile does not already exist
            var fileNames = String.Join(
                               ", ",
                               Directory.GetFiles(UserProfileDirectoryPath, "*.xml")
                                  .Select(filename =>
                                      Path.GetFileNameWithoutExtension(filename))
                                  .ToArray());
            if (fileNames.Contains(nameParam))
            {
                var errorWindow = CollectiveConsciousGuiController.Instance.GetLogInErrorWindow("User Profile with that name already exists.");
                var x = (GuiGraphicsScreen)Microsoft.Practices.ServiceLocation.ServiceLocator.Current.GetInstance<IGraphicsService>().Screens[0];
                x.UIScreen.Children.Add(errorWindow);
            }
            else
            {
                //IF IT DOESNT:
                CollectiveConsciousUserProfile newProfile = new CollectiveConsciousUserProfile();
                newProfile.ProfName = nameParam;
                newProfile.eegProfileData = EEGEngine.GetUserProfile(_userID).GetBytes();
                newProfile.FearDataHistory = new List<FearData>();

                #region Contrived Data (commented out for testing purposes)
                //FearData fdtestA = new FearData();
                //fdtestA.EEGBrainWaveValues = new List<float>();
                //fdtestA.Date = new DateTime(2013, 12, 25);
                //fdtestA.EEGBrainWaveValues.Add(0.234f);
                //fdtestA.EEGBrainWaveValues.Add(0.153f);
                //fdtestA.EEGBrainWaveValues.Add(0.577f);
                //fdtestA.EEGBrainWaveValues.Add(0.788f);
                //fdtestA.EEGBrainWaveValues.Add(0.399f);

                //FearData fdtestB = new FearData();
                //fdtestB.EEGBrainWaveValues = new List<float>();
                //fdtestB.Date = new DateTime(2014, 2, 9);
                //fdtestB.EEGBrainWaveValues.Add(0.01f);
                //fdtestB.EEGBrainWaveValues.Add(0.10f);
                //fdtestB.EEGBrainWaveValues.Add(0.3f);
                //fdtestB.EEGBrainWaveValues.Add(0.8f);
                //fdtestB.EEGBrainWaveValues.Add(0.99f);

                //FearData fdtestC = new FearData();
                //fdtestC.EEGBrainWaveValues = new List<float>();
                //fdtestC.Date = new DateTime(2014, 2, 11);
                //fdtestC.EEGBrainWaveValues.Add(0.77f);
                //fdtestC.EEGBrainWaveValues.Add(0.56f);
                //fdtestC.EEGBrainWaveValues.Add(0.23f);
                //fdtestC.EEGBrainWaveValues.Add(0.15f);
                //fdtestC.EEGBrainWaveValues.Add(0.38f);

                //FearData fdtestD = new FearData();
                //fdtestD.EEGBrainWaveValues = new List<float>();
                //fdtestD.Date = new DateTime(2014, 2, 12);
                //fdtestD.EEGBrainWaveValues.Add(0.26f);
                //fdtestD.EEGBrainWaveValues.Add(0.63f);
                //fdtestD.EEGBrainWaveValues.Add(0.45f);
                //fdtestD.EEGBrainWaveValues.Add(0.87f);
                //fdtestD.EEGBrainWaveValues.Add(0.73f);

                //FearData fdtestE = new FearData();
                //fdtestE.EEGBrainWaveValues = new List<float>();
                //fdtestE.Date = new DateTime(2014, 3, 4);
                //fdtestE.EEGBrainWaveValues.Add(0.45f);
                //fdtestE.EEGBrainWaveValues.Add(0.88f);
                //fdtestE.EEGBrainWaveValues.Add(0.32f);
                //fdtestE.EEGBrainWaveValues.Add(0.23f);
                //fdtestE.EEGBrainWaveValues.Add(0.56f);

                //FearData fdtestF = new FearData();
                //fdtestF.EEGBrainWaveValues = new List<float>();
                //fdtestF.Date = new DateTime(2014, 3, 20);
                //fdtestF.EEGBrainWaveValues.Add(0.45f);
                //fdtestF.EEGBrainWaveValues.Add(0.67f);
                //fdtestF.EEGBrainWaveValues.Add(0.77f);
                //fdtestF.EEGBrainWaveValues.Add(0.87f);
                //fdtestF.EEGBrainWaveValues.Add(0.63f);

                //FearData fdtestG = new FearData();
                //fdtestG.EEGBrainWaveValues = new List<float>();
                //fdtestG.Date = new DateTime(2014, 4, 29);
                //fdtestG.EEGBrainWaveValues.Add(0.12f);
                //fdtestG.EEGBrainWaveValues.Add(0.33f);
                //fdtestG.EEGBrainWaveValues.Add(0.65f);
                //fdtestG.EEGBrainWaveValues.Add(0.23f);
                //fdtestG.EEGBrainWaveValues.Add(0.53f);

                //FearData fdtestH = new FearData();
                //fdtestH.EEGBrainWaveValues = new List<float>();
                //fdtestH.Date = new DateTime(2014, 5, 7);
                //fdtestH.EEGBrainWaveValues.Add(0.43f);
                //fdtestH.EEGBrainWaveValues.Add(0.56f);
                //fdtestH.EEGBrainWaveValues.Add(0.67f);
                //fdtestH.EEGBrainWaveValues.Add(0.99f);
                //fdtestH.EEGBrainWaveValues.Add(0.8f);

                //FearData fdtestI = new FearData();
                //fdtestI.EEGBrainWaveValues = new List<float>();
                //fdtestI.Date = new DateTime(2014, 5, 16);
                //fdtestI.EEGBrainWaveValues.Add(1.0f);
                //fdtestI.EEGBrainWaveValues.Add(1.0f);
                //fdtestI.EEGBrainWaveValues.Add(0.8f);
                //fdtestI.EEGBrainWaveValues.Add(1.0f);
                //fdtestI.EEGBrainWaveValues.Add(0.74f);

                //FearData fdtestJ = new FearData();
                //fdtestJ.EEGBrainWaveValues = new List<float>();
                //fdtestJ.Date = new DateTime(2014, 6, 2);
                //fdtestJ.EEGBrainWaveValues.Add(0.45f);
                //fdtestJ.EEGBrainWaveValues.Add(0.56f);
                //fdtestJ.EEGBrainWaveValues.Add(0.65f);
                //fdtestJ.EEGBrainWaveValues.Add(0.76f);
                //fdtestJ.EEGBrainWaveValues.Add(0.87f);
                //fdtestJ.EEGBrainWaveValues.Add(0.43f);
                //fdtestJ.EEGBrainWaveValues.Add(0.56f);
                //fdtestJ.EEGBrainWaveValues.Add(0.67f);
                //fdtestJ.EEGBrainWaveValues.Add(0.99f);
                //fdtestJ.EEGBrainWaveValues.Add(0.8f);

                //FearData fdtestK = new FearData();
                //fdtestK.EEGBrainWaveValues = new List<float>();
                //fdtestK.Date = new DateTime(2014, 6, 6);
                //fdtestK.EEGBrainWaveValues.Add(0.23f);
                //fdtestK.EEGBrainWaveValues.Add(0.11f);
                //fdtestK.EEGBrainWaveValues.Add(0.01f);
                //fdtestK.EEGBrainWaveValues.Add(0.12f);
                //fdtestK.EEGBrainWaveValues.Add(0.89f);

                //FearData fdtestL = new FearData();
                //fdtestL.EEGBrainWaveValues = new List<float>();
                //fdtestL.Date = DateTime.Now;
                //fdtestL.EEGBrainWaveValues.Add(0.0f);
                //fdtestL.EEGBrainWaveValues.Add(0.6f);
                //fdtestL.EEGBrainWaveValues.Add(0.3f);
                //fdtestL.EEGBrainWaveValues.Add(0.5f);
                //fdtestL.EEGBrainWaveValues.Add(0.4f);

                //FearData fdtestM = new FearData();
                //fdtestM.EEGBrainWaveValues = new List<float>();
                //fdtestM.Date = DateTime.Now;
                //fdtestM.EEGBrainWaveValues.Add(0.45f);
                //fdtestM.EEGBrainWaveValues.Add(0.67f);
                //fdtestM.EEGBrainWaveValues.Add(0.77f);
                //fdtestM.EEGBrainWaveValues.Add(0.87f);
                //fdtestM.EEGBrainWaveValues.Add(0.63f);

                //FearData fdtestN = new FearData();
                //fdtestN.EEGBrainWaveValues = new List<float>();
                //fdtestN.Date = DateTime.Now;
                //fdtestN.EEGBrainWaveValues.Add(0.45f);
                //fdtestN.EEGBrainWaveValues.Add(0.67f);
                //fdtestN.EEGBrainWaveValues.Add(0.77f);
                //fdtestN.EEGBrainWaveValues.Add(0.87f);
                //fdtestN.EEGBrainWaveValues.Add(0.63f);

                //FearData fdtestO = new FearData();
                //fdtestO.EEGBrainWaveValues = new List<float>();
                //fdtestO.Date = DateTime.Now;
                //fdtestO.EEGBrainWaveValues.Add(0.45f);
                //fdtestO.EEGBrainWaveValues.Add(0.67f);
                //fdtestO.EEGBrainWaveValues.Add(0.77f);
                //fdtestO.EEGBrainWaveValues.Add(0.87f);
                //fdtestO.EEGBrainWaveValues.Add(0.63f);

                //FearData fdtestP = new FearData();
                //fdtestP.EEGBrainWaveValues = new List<float>();
                //fdtestP.Date = DateTime.Now;
                //fdtestP.EEGBrainWaveValues.Add(0.23f);
                //fdtestP.EEGBrainWaveValues.Add(0.33f);
                //fdtestP.EEGBrainWaveValues.Add(0.11f);
                //fdtestP.EEGBrainWaveValues.Add(0.55f);
                //fdtestP.EEGBrainWaveValues.Add(0.44f);

                //FearData fdtestQ = new FearData();
                //fdtestQ.EEGBrainWaveValues = new List<float>();
                //fdtestQ.Date = DateTime.Now;
                //fdtestQ.EEGBrainWaveValues.Add(0.9f);
                //fdtestQ.EEGBrainWaveValues.Add(0.7f);
                //fdtestQ.EEGBrainWaveValues.Add(0.65f);
                //fdtestQ.EEGBrainWaveValues.Add(0.75f);
                //fdtestQ.EEGBrainWaveValues.Add(0.55f);

                //FearData fdtestR = new FearData();
                //fdtestR.EEGBrainWaveValues = new List<float>();
                //fdtestR.Date = DateTime.Now;
                //fdtestR.EEGBrainWaveValues.Add(0.34f);
                //fdtestR.EEGBrainWaveValues.Add(0.54f);
                //fdtestR.EEGBrainWaveValues.Add(0.66f);
                //fdtestR.EEGBrainWaveValues.Add(0.99f);
                //fdtestR.EEGBrainWaveValues.Add(0.23f);

                //newProfile.FearDataHistory.Add(fdtestA);
                //newProfile.FearDataHistory.Add(fdtestB);
                //newProfile.FearDataHistory.Add(fdtestC);
                //newProfile.FearDataHistory.Add(fdtestD);
                //newProfile.FearDataHistory.Add(fdtestE);
                //newProfile.FearDataHistory.Add(fdtestF);
                //newProfile.FearDataHistory.Add(fdtestG);
                //newProfile.FearDataHistory.Add(fdtestH);
                //newProfile.FearDataHistory.Add(fdtestI);
                //newProfile.FearDataHistory.Add(fdtestJ);
                //newProfile.FearDataHistory.Add(fdtestK);
                //newProfile.FearDataHistory.Add(fdtestL);
                //newProfile.FearDataHistory.Add(fdtestM);
                //newProfile.FearDataHistory.Add(fdtestN);
                //newProfile.FearDataHistory.Add(fdtestO);
                //newProfile.FearDataHistory.Add(fdtestP);
                //newProfile.FearDataHistory.Add(fdtestQ);
                //newProfile.FearDataHistory.Add(fdtestR);

                #endregion

                //Persist the profile
                XmlManager<CollectiveConsciousUserProfile> xml = new XmlManager<CollectiveConsciousUserProfile>();
                xml.Save(UserProfileDirectoryPath + nameParam + ".xml", newProfile);

                //load the profile
                UserProfile = newProfile;
                _profileName = nameParam;
            }
        }

        public void LogOut()
        {
            if (IsUserLoggedIn())
            {
                Save();
                _profileName = string.Empty;
            }
        }

        public void LogIn(string profileNameParam)
        {
            _profileName = profileNameParam;
            XmlManager<CollectiveConsciousUserProfile> xml = new XmlManager<CollectiveConsciousUserProfile>();
            UserProfile = xml.Load(UserProfileDirectoryPath + _profileName + ".xml");
        }

        public bool IsHeadSetOn()
        {
            return (CurrentState == null) ? false : (CurrentState.GetWirelessSignalStatus() == EdkDll.EE_SignalStrength_t.NO_SIGNAL) ? false : true;
        }

        public EdkDll.EE_EEG_ContactQuality_t GetAverageQuality()
        {
            EdkDll.EE_EEG_ContactQuality_t result = EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_NO_SIGNAL;

            if (CurrentState != null && IsHeadSetOn())
            {
                var enumList = CurrentState.GetContactQualityFromAllChannels();
                float currentAverage = 0;
                foreach(EdkDll.EE_EEG_ContactQuality_t c in enumList)
                {
                    currentAverage += (int)c;
                }
                currentAverage = currentAverage / 14;

                result = (currentAverage > 3.5) ? EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_GOOD
                    : (currentAverage > 2.5) ? EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_FAIR
                    : (currentAverage > 1.5) ? EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_POOR
                    : (currentAverage > .5) ? EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_VERY_BAD
                    : EdkDll.EE_EEG_ContactQuality_t.EEG_CQ_NO_SIGNAL;
            }
            
            return result;
        }

        public void Save()
        {
            XmlManager<CollectiveConsciousUserProfile> xml = new XmlManager<CollectiveConsciousUserProfile>();
            xml.Save(UserProfileDirectoryPath + _profileName + ".xml", UserProfile);
        }


        #endregion

        //--------------------------------------------------------------
        #region Emotive Delegate Registration
        //--------------------------------------------------------------
        private void registerDelegates()
        {
            EEGEngine.UserAdded += InstanceOnUserAdded;
            EEGEngine.UserRemoved += InstanceOnUserRemoved;
            EEGEngine.EmoStateUpdated += InstanceOnEmoStateUpdated;
            //EEGEngine.AffectivEmoStateUpdated += InstanceOnEmoStateUpdated;
            EEGEngine.EmoEngineEmoStateUpdated += InstanceOnEmoStateUpdated;

        }

        private void InstanceOnEmoStateUpdated(object sender, EmoStateUpdatedEventArgs eventArgs)
        {
            lock (EEGEngine)
            {
                CurrentState = eventArgs.emoState;
                if (IsHeadSetOn() && CollectiveConsciousGuiController.Instance.CurrentlyRecording)
                {
                    var boredomFloat = eventArgs.emoState.AffectivGetEngagementBoredomScore();
                    var longTermExcitementFloat = eventArgs.emoState.AffectivGetExcitementLongTermScore();
                    var shortTermExcitementFloat = eventArgs.emoState.AffectivGetExcitementShortTermScore();
                    var frustrationFloat = eventArgs.emoState.AffectivGetFrustrationScore();
                    var meditationFloat = eventArgs.emoState.AffectivGetMeditationScore();

                    float fear = (((2 / 3) * boredomFloat + (1 / 3) * longTermExcitementFloat + 2 * shortTermExcitementFloat + 2 * frustrationFloat - meditationFloat) / 4);
                    fear = (fear < 0) ? 0 : fear;
                    CollectiveConsciousGuiController.Instance.currentRecordingFearData.EEGBrainWaveValues.Add(fear);
                    CollectiveConsciousGuiController.Instance.Is_FearDataToBeGraphed_Changed = true;
                }
                
            }
        }

        private void InstanceOnUserRemoved(object sender, EmoEngineEventArgs eventArgs)
        {
            LogOut();
            USBConnected = false;
        }

        private void InstanceOnUserAdded(object sender, EmoEngineEventArgs eventArgs)
        {
            USBConnected = true;
        }

        private void EngineUpdate()
        {
            while (true)
            {
                Emotiv.EmoEngine.Instance.ProcessEvents(_emoEnginePollingPeriod);
                Thread.Sleep(_processTime);
            }
        }

        #endregion

    }
}
