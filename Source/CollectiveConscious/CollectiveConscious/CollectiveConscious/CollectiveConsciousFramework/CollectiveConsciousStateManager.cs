﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using DigitalRune.Animation;
using DigitalRune.Game.Input;
using DigitalRune.Game.States;
using DigitalRune.Game.UI;
using DigitalRune.Game.UI.Controls;
using DigitalRune.Game.UI.Rendering;
using DigitalRune.Geometry.Shapes;
using DigitalRune.Graphics;
using DigitalRune.Mathematics.Algebra;
using DigitalRune.Threading;
using DigitalRune.ServiceLocation;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Color = Microsoft.Xna.Framework.Color;
using Image = DigitalRune.Game.UI.Controls.Image;
using CollectiveConscious.CollectiveConsciousModelObjects;



namespace CollectiveConscious.CollectiveConsciousFramework
{
    class CollectiveConsciousStateManager : GameComponent
    {

        //--------------------------------------------------------------
        #region Instance Members
        //--------------------------------------------------------------
        private readonly ServiceContainer _services;
        private readonly IInputService _inputService;
        private readonly IGraphicsService _graphicsService;
        private readonly SpriteBatch _spriteBatch;
        private readonly SpriteFont _spriteFont;
        private ContentManager UIContentManager;
        private Game _game;


        //GUI controls.
        private GuiGraphicsScreen _guiGraphicsScreen;
        private DelegateGraphicsScreen _mainScreen;
        private DelegateGraphicsScreen _eEGConScreen;
        private Window _updateWindow;
        private Window _menuWindow;
        private Window _logInWindow;
        private readonly Color COLOR_TRANSPARENT = Color.White;
        private readonly int APPLICATION_WIDTH = 1280;
        private readonly int APPLICATION_HEIGHT = 720;
        //private readonly int MAINWINDOW_WIDTH = 1080;
        //private readonly int SUBSCREEN_WIDTH = 200;
        //private readonly float SUBSCREEN_ROTATION = 0f;
        private readonly Vector2 SUBSCREEN_ORIGIN = new Vector2(0, 0);
        private readonly Vector2 SUBSCREEN_POSITION = new Vector2(0, 0);
        private readonly Vector2 SUBSCREEN_SCALE = new Vector2(1, 1);
        //private readonly Microsoft.Xna.Framework.Graphics.SpriteEffects SUBSCREEN_EFFECTS = Microsoft.Xna.Framework.Graphics.SpriteEffects.None;
        //private readonly float SUBSCREEN_DEPTH = 1;
        private readonly int MAINWINDOW_WIDTH = 1080;

        //StateMachine has a "state" property, and an "update" method.
        private StateMachine _stateMachine;
        //Bool used in systemVerificationCheckState transition
        private volatile bool _systemUpToDate;
        #endregion

        //--------------------------------------------------------------
        #region Constructor and Project Initialization
        //--------------------------------------------------------------

        #region Constructor
        public CollectiveConsciousStateManager(Game game)
            : base(game)
        {
            //Setting Services.
            _services = (ServiceContainer)ServiceLocator.Current;
            _inputService = _services.GetInstance<IInputService>();
            _graphicsService = _services.GetInstance<IGraphicsService>();
            UIContentManager = (ContentManager)_services.GetInstance(typeof(ContentManager), "UIContent");
            
            _mainScreen = new DelegateGraphicsScreen(_graphicsService)
            {
                Name = "MainScreen",
                Coverage = GraphicsScreenCoverage.Partial,
                IsVisible = false,
                RenderPreviousScreensToTexture = true,
                SourceTextureFormat = new RenderTargetFormat(MAINWINDOW_WIDTH, APPLICATION_HEIGHT, false, SurfaceFormat.Color, DepthFormat.Depth24),
            };
            CollectiveConsciousGuiController.Instance.MainScreen = _mainScreen;
            _game = game;

            // Create a sprite batch (useful for GUI rendering with different Screen Objects)
            _spriteBatch = new SpriteBatch(_graphicsService.GraphicsDevice);

            // Load a sprite font.
            _spriteFont = UIContentManager.Load<SpriteFont>("Default");

            // Update this game component after all other game components. (The other
            // game components should get a chance to handle input first.)
            UpdateOrder = int.MaxValue;


            //Call other neccesary Initialization methods.
            InitializeGui();
            InitializeEEGManager();
            InitializeStates();

        }

        #endregion

        #region Initialize Gui
        private void InitializeGui()
        {

            #region Application Screen
            // Add the GuiGraphicsScreen to the graphics service.
            _guiGraphicsScreen = new GuiGraphicsScreen(_services,"applicationScreen");
            _graphicsService.Screens.Insert(0, _guiGraphicsScreen);
            _guiGraphicsScreen.UIScreen.Background = new Color(100, 100, 100);
            #endregion

            #region Update Window Initialization

            _updateWindow = CollectiveConsciousGuiController.Instance.GetUpdateWindow();

            #endregion

            #region EEG Connection Screen

            _eEGConScreen = new DelegateGraphicsScreen(_graphicsService)
            {
                Name = "Brain Interface Connection Status",
                RenderCallback = RenderEEGConScreen,
                Coverage = GraphicsScreenCoverage.Partial,
                IsVisible = false,


                //The renderTargetFormat width and height are to scale the selected region to a regular application size.
                //This was a work-around to a nested screen problem that I encoutered, though there might be more intelligent solutions to the problem.
                SourceTextureFormat = new RenderTargetFormat(APPLICATION_WIDTH, APPLICATION_HEIGHT, false, SurfaceFormat.Color, DepthFormat.Depth24),
            };
            _graphicsService.Screens.Insert(1, _eEGConScreen);

            #endregion

            #region Main Screen

            _graphicsService.Screens.Insert(2, _mainScreen);

            #endregion

            /*
             *Start by entering into the 
             *  (state engine will handle controll flow from there until the applicaiton reaches the menu state...
             *  at which point control flow will be dictated through user input.
             */
            _updateWindow.Show(_guiGraphicsScreen.UIScreen);

        }
        #endregion

        #region Initialize EEGManager
        //---------------------------
        public void InitializeEEGManager()
        {
            //Anything that needs to be initialized in the EEG Manager should be done here.
        }

        #endregion

        #endregion

        //--------------------------------------------------------------
        #region StateM. and Transition Initialization, and State Events
        //--------------------------------------------------------------
        private void InitializeStates()
        {

            #region State Machine & States
            //Initialize State machine
            _stateMachine = new StateMachine();

            var systemVersionCheckState = new State { Name = "SystemVersionCheck" };
            //Adding events for the Version Check State
            systemVersionCheckState.Enter += OnEnterVersionCheck;
            systemVersionCheckState.Exit += OnExitVersionCheck;
            _stateMachine.States.Add(systemVersionCheckState);

            var systemVersionVerifiedState = new State { Name = "SystemVersionVerified" };
            //Adding events for the Version Verified State
            systemVersionVerifiedState.Enter += OnEnterVersionVerified;
            systemVersionVerifiedState.Update += OnUpdateVersionVerified;
            systemVersionVerifiedState.Exit += OnExitVersionVerified;
            _stateMachine.States.Add(systemVersionVerifiedState);


            var logInState = new State { Name = "LogIn" };
            //adding events for the Log In State
            logInState.Enter += OnEnterLogInState;
            logInState.Update += OnUpdateLogInState;
            logInState.Exit += OnExitLogInState;
            _stateMachine.States.Add(logInState);

            var menuState = new State { Name = "Menu" };
            //Adding events for the Menu State
            menuState.Enter += OnEnterMenuScreen;
            menuState.Update += OnUpdateMenuScreen;
            menuState.Exit += OnExitMenuScreen;
            _stateMachine.States.Add(menuState);

            #endregion

            #region Transitions Between States

            //Transion between Version Check State and Version Verified State"
            var versionCheckToVersionVerifiedTransition = new Transition
            {
                Name = "VersionCheckToVersionVerified",
                TargetState = systemVersionVerifiedState,
                FireAlways = true,
                Guard = () => _systemUpToDate,
                /*
                 * Now, because of the [FireAlways] the transition is really just wating for the guard condition to equal true.
                 * A soon as the LoadAssets method of the Loading State sets the _allAssetsLoaded bool to true, this transition will fire.
                 */
            };
            systemVersionCheckState.Transitions.Add(versionCheckToVersionVerifiedTransition);

            //Transion between Version Verified State and Log In State"
            var versionVerifiedToLogInTransition = new Transition
            {
                Name = "VersionVerifiedToLogInTransition",
                TargetState = logInState,
            };
            systemVersionVerifiedState.Transitions.Add(versionVerifiedToLogInTransition);

            //Transition between Log In State and Menu State
            var logInToMenuTransition = new Transition
            {
                Name = "LogInToMenuTransition",
                TargetState = menuState,
            };
            logInState.Transitions.Add(logInToMenuTransition);

            //Transition between Menu State and Log In State
            var menuToLogInTransition = new Transition
            {
                Name = "MenuToLogInTransition",
                TargetState = logInState,
            };
            menuState.Transitions.Add(menuToLogInTransition);

            #endregion

        }
        //--------------------------
        #region State Event Methods
        //--------------------------
        #region Version Check Events

        private TextBlock _versionCheckTextBlock;  //The Text that is going to display on screen during the loading State.
        private Image _versionCheckGraphic;

        //The event is called when "Version Check is Entered"
        private void OnEnterVersionCheck(object sender, StateEventArgs eventArgs)
        {

            //Display the _versionCheckTextBlock
            _versionCheckTextBlock = new TextBlock
            {
                Name = "VersionCheckTextBlock", // Control names, while optional, really assist in debuging.
                Text = "Checking for Application Updates...",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                IsVisible = false,
            };

            //Display the _versionCheckGraphic
            _versionCheckGraphic = new Image
            {
                Texture = Game.Content.Load<Texture2D>("Images/oneMind"),
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
            };

            //Add visual components To The Loading Window.
            var tempStack = _updateWindow.Content as StackPanel;
            tempStack.Children.Add(_versionCheckGraphic);
            tempStack.Children.Add(_versionCheckTextBlock);

            // Start loading assets in the background.
            Parallel.StartBackground(CheckForUpdates);
        }


        //The following method will check for Updates.
        private void CheckForUpdates()
        {
            //The following wait is completely arbitrary... just a placeholder updateCheck process; simulating 4 second check.
            // TODO: Impliment Update UserStory
            Thread.Sleep(TimeSpan.FromSeconds(1));
            _versionCheckTextBlock.IsVisible = true;
            Thread.Sleep(TimeSpan.FromSeconds(1));
            _systemUpToDate = true;
        }

        //The event is called when "Version Check is Exited"
        private void OnExitVersionCheck(object sender, StateEventArgs eventArgs)
        {
            // Clean up by removoing the "Loading Screen" form the _screen's Children components.
            var tempStack = _updateWindow.Content as StackPanel;
            tempStack.Children.Remove(_versionCheckTextBlock);
            tempStack.Children.Remove(_versionCheckGraphic);
            _versionCheckTextBlock = null;
            _versionCheckGraphic = null;
        }


        #endregion

        #region Version Verified Events

        private TextBlock _versionVerifiedTextBlock; // The Text that displays on the screen durring the Start Screen State

        //The event is called when "Version Verified is Entered"
        private void OnEnterVersionVerified(object sender, StateEventArgs eventArgs)
        {
            // The _versionVerifiedTextBlock will be displayed on the screen, centered.
            _versionVerifiedTextBlock = new TextBlock
            {
                Name = "VersionVerifiedTextBlock",
                Text = "Your application is fully up to date.\nPress Enter to Continue.",
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
            };
            //Add the VersionVerified Textblock to the Update Window. 
            var tempStack = _updateWindow.Content as StackPanel;
            tempStack.Children.Add(_versionVerifiedTextBlock);
        }

        //The event is called every fram during "Version Verified State"
        private void OnUpdateVersionVerified(object sender, StateEventArgs eventArgs)
        {
            // Check if the user has pressed the left mouse button, or an enter key, escape key, or spacebar key..
            bool transitionToMenu = (_inputService.IsDown(Keys.Enter)
                || _inputService.IsDown(Keys.Escape)
                || _inputService.IsDown(Keys.Space)) ? true : false;

            if (transitionToMenu)
            {
                _stateMachine.States.ActiveState.Transitions["VersionVerifiedToLogInTransition"].Fire();
            }
        }

        //The event is called when "Version Verified is Exited"
        private void OnExitVersionVerified(object sender, StateEventArgs eventArgs)
        {
            //Clean up the state vars from the screen
            //_guiGraphicsScreen.UIScreen.Children.Remove(_versionVerifiedTextBlock);
            var tempStack = _updateWindow.Content as StackPanel;
            tempStack.Children.Remove(_versionVerifiedTextBlock);

            _versionVerifiedTextBlock = null;
            _updateWindow.Close();
            _updateWindow = null;

            _eEGConScreen.IsVisible = true;

        }

        #endregion

        #region Log In Events

        private void OnEnterLogInState(object sender, StateEventArgs eventArgs)
        {
            _mainScreen.IsVisible = false;

            if (_menuWindow != null)
            {
                _menuWindow.Close();
                _menuWindow = null;
            }
            _menuWindow = CollectiveConsciousGuiController.Instance.GetMenuWindow();
            _menuWindow.Show(_guiGraphicsScreen.UIScreen);
            _logInWindow = CollectiveConsciousGuiController.Instance.GetLogInWindow();
            _logInWindow.Show(_guiGraphicsScreen.UIScreen);
        }

        //The event is called every fram during "Log In State"
        private void OnUpdateLogInState(object sender, StateEventArgs eventArgs)
        {
            _logInWindow.IsVisible = EEGManager.Instance.USBConnected;

            if (EEGManager.Instance.IsUserLoggedIn())
            {
                _stateMachine.States.ActiveState.Transitions["LogInToMenuTransition"].Fire();
            }
        }

        //The event is called when "Log-in State is Exited"
        private void OnExitLogInState(object sender, StateEventArgs eventArgs)
        {
            //Clean up the state vars from the screen
            _mainScreen.IsVisible = true;

            _menuWindow.Close();
            _menuWindow = null;

            _logInWindow.Close();
            _logInWindow = null;

        }

        #endregion

        #region Menu Events

        private void OnEnterMenuScreen(object sender, StateEventArgs eventArgs)
        {
            if (_menuWindow != null)
            {
                _menuWindow.Close();
                _menuWindow = null;
            }
            _menuWindow = CollectiveConsciousGuiController.Instance.GetMenuWindow();
            _menuWindow.Show(_guiGraphicsScreen.UIScreen);

        }


        //The event is called every fram during "Version Verified State"
        private void OnUpdateMenuScreen(object sender, StateEventArgs eventArgs)
        {
            if (!EEGManager.Instance.IsUserLoggedIn())
            {
                _stateMachine.States.ActiveState.Transitions["MenuToLogInTransition"].Fire();
            }
        }

        //The event is called when "Version Verified is Exited"
        private void OnExitMenuScreen(object sender, StateEventArgs eventArgs)
        {
            _menuWindow.Close();
            _menuWindow = null;
        }

        #endregion

        //--------------------------
        #endregion



        #endregion

        //--------------------------------------------------------------
        #region Project GameComponent Methods (Update, Multiple-Screen rendering etc.)

        public override void Update(GameTime gameTimeParam)
        {
            _stateMachine.Update(gameTimeParam.ElapsedGameTime);
        }

        #region Sub Screen Render Functions.

        // Renders the content of the EEG Connection Status screen.
        private void RenderEEGConScreen(RenderContext context)
        {
            var graphicsDevice = context.GraphicsService.GraphicsDevice;

            _spriteBatch.Begin();
            //<Menu Screen Renders>
            if (EEGManager.Instance.IsUserLoggedIn())
            {
                if (!EEGManager.Instance.IsHeadSetOn())
                {
                    CollectiveConsciousGuiController.Instance.HideMenuFunctions();
                }
                else
                {
                    if (!CollectiveConsciousGuiController.Instance.CurrentlyRecording)
                    {
                        CollectiveConsciousGuiController.Instance.UnHideMenuFunctions();
                    }
                }
            }




            //EEG-CON Screen Background Render
            Texture2D blackBackgroundTexture = _game.Content.Load<Texture2D>("Images/Technology_Background");
            _spriteBatch.Draw(blackBackgroundTexture, new Microsoft.Xna.Framework.Rectangle(0, 520, 200, 200), Color.Black);

            //Then EEGWindow Element Render   (Ugly if/else block; will refactor as time allows)
            if (!EEGManager.Instance.USBConnected)
            {
                _spriteBatch.DrawString(_spriteFont, "Plug in EEG USB", new Vector2(40, 615), Color.White);
            }
            else if (EEGManager.Instance.USBConnected && !EEGManager.Instance.IsUserLoggedIn())
            {
                _spriteBatch.DrawString(_spriteFont, "Load a Profile to proceed", new Vector2(25, 615), Color.White);
            }
            else if (EEGManager.Instance.CurrentState != null)
            {
                if (EEGManager.Instance.IsHeadSetOn())
                {

                    int firstNerdVariable = 0;
                    int secondNerdVariable = 100;
                    EEGManager.Instance.CurrentState.GetBatteryChargeLevel(out firstNerdVariable, out secondNerdVariable);
                    _spriteBatch.DrawString(_spriteFont, "Contact Quality:  " + EEGManager.Instance.GetAverageQuality().ToString().Substring(7), new Vector2(1, 550), Color.White);
                    //_spriteBatch.DrawString(_spriteFont, "Batery Level:     " + ((firstNerdVariable / secondNerdVariable) * 100), new Vector2(1, 575), Color.White);
                    _spriteBatch.DrawString(_spriteFont, "Current User:     " + EEGManager.Instance.UserProfile.ProfName, new Vector2(1, 600), Color.White);
                    _spriteBatch.DrawString(_spriteFont, "Time Since Start: " + EEGManager.Instance.CurrentState.GetTimeFromStart(), new Vector2(1, 625), Color.White);
                }
                else
                {
                    _spriteBatch.DrawString(_spriteFont, "Turn on your EEGHeadSet", new Vector2(25, 615), Color.White);
                }
            }
            else
            {
                _spriteBatch.DrawString(_spriteFont, "Turn on your EEGHeadSet", new Vector2(25, 615), Color.White);
            }

            _spriteBatch.End();
        }

        #endregion


        #endregion

    }
}
