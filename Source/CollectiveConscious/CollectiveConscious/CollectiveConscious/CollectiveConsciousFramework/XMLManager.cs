﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace CollectiveConscious.CollectiveConsciousFramework
{
    public class XmlManager<T>
    {

        /*
         * Generic Xml Manager Class developed durring one of my earliest XNA tutorials. 
         */ 

        public Type Type { get; set; }

        public XmlManager()
        {
            Type = typeof(T);
        }

        public T Load(string pathParam)
        {
            T instance;
            using (TextReader reader = new StreamReader(pathParam))
            {

                XmlSerializer xmlSerializer = new XmlSerializer(Type);
                instance = (T)xmlSerializer.Deserialize(reader);
            }

            return instance;
        }

        public void Save(string pathParam, object objParam)
        {
            using (TextWriter writer = new StreamWriter(pathParam))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(Type);
                xmlSerializer.Serialize(writer, objParam);
            }
        }
    }
}
