﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalRune.Game.Input;
using DigitalRune.Mathematics.Algebra;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CollectiveConscious.Vendor
{
    class MouseComponent : GameComponent
    {

        private readonly IInputService _inputService;


        public bool EnableMouseCentering
        {
            get { return _enableMouseCentering; }
            set { _enableMouseCentering = value; }
        }
        private bool _enableMouseCentering;


        public MouseComponent(Microsoft.Xna.Framework.Game game)
            : base(game)
        {
            _inputService = ServiceLocator.Current.GetInstance<IInputService>();
            
            // Set the mouse centering position to center of the game window.
            var presentationParameters = Game.GraphicsDevice.PresentationParameters;
            _inputService.Settings.MouseCenter = new Vector2F(
              presentationParameters.BackBufferWidth / 2.0f,
              presentationParameters.BackBufferHeight / 2.0f);

            // By default, enable mouse centering and hide mouse cursor.
            EnableMouseCentering = true;

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                EnableMouseCentering = false;

            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
            bool isAltDown = _inputService.IsDown(Keys.LeftAlt) || _inputService.IsDown(Keys.RightAlt);
            bool isControlDown = _inputService.IsDown(Keys.LeftControl) || _inputService.IsDown(Keys.RightControl);
            var temp = Game as CollectiveConsciousDriver;
            bool isVRActive = temp.IsVRActive();
            bool isMouseVisible = isControlDown || isAltDown || !Game.IsActive || !isVRActive;

            _inputService.EnableMouseCentering = _enableMouseCentering && !isMouseVisible;
            Game.IsMouseVisible = !_enableMouseCentering || isMouseVisible;
        }

        
    }
}
