using System;

namespace CollectiveConscious
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            using (CollectiveConsciousDriver game = new CollectiveConsciousDriver())
            {
                game.Run();
            }
        }
    }
#endif
}

