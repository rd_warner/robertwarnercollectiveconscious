﻿using System;
using DigitalRune.Geometry;
using DigitalRune.Graphics;
using DigitalRune.Graphics.Rendering;
using DigitalRune.Graphics.SceneGraph;
using DigitalRune.Mathematics;
using DigitalRune.Mathematics.Algebra;
using DigitalRune.Mathematics.Statistics;
using DigitalRune.Physics.ForceEffects;
using Microsoft.Xna.Framework;
using MathHelper = Microsoft.Xna.Framework.MathHelper;
using OculusRift;
using CollectiveConscious.Vendor;


namespace CollectiveConscious.FDVEs
{
    public class AlphaFDVE : FDVE
    {

        private readonly SplitScreen _oculusScreen;

        //The second camera.
        private readonly CameraNode _cameraNodeB;


        public AlphaFDVE(Microsoft.Xna.Framework.Game game)
            : base(game)
        {

            _oculusScreen = new SplitScreen(Services);
            _oculusScreen.DrawReticle = true;
            GraphicsService.Screens.Insert(3, _oculusScreen);

            Services.Register(typeof(DebugRenderer), null, _oculusScreen.DebugRenderer);
            Services.Register(typeof(IScene), null, _oculusScreen.Scene);

            Simulation.ForceEffects.Add(new Gravity());
            Simulation.ForceEffects.Add(new Damping());

            // Add a custom game object which controls the camera of player A.
            var cameraGameObject = new CameraObject(Services);
            GameObjectService.Objects.Add(cameraGameObject);
            _oculusScreen.CameraNode = cameraGameObject.CameraNode;

            var projection = (PerspectiveProjection)cameraGameObject.CameraNode.Camera.Projection;
            projection.SetFieldOfView(
              projection.FieldOfViewY,
              GraphicsService.GraphicsDevice.Viewport.AspectRatio / 2,
              projection.Near,
              projection.Far);
            cameraGameObject.CameraNode.Camera = new Camera(projection);

            // A second camera for player B.
            _cameraNodeB = new CameraNode(cameraGameObject.CameraNode.Camera);
            _oculusScreen.ActiveCameraNodeB = _cameraNodeB;

            GameObjectService.Objects.Add(new GroundObject(Services));
            GameObjectService.Objects.Add(new StaticSkyObject(Services));

           
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Clean up.
                _cameraNodeB.Dispose(false);

                GameObjectService.Objects.Clear();

                Simulation.RigidBodies.Clear();
                Simulation.ForceEffects.Clear();

                _oculusScreen.GraphicsService.Screens.Remove(_oculusScreen);
                _oculusScreen.Dispose();
            }

            base.Dispose(disposing);
        }


        public override void Update(GameTime gameTime)
        {
            // This sample clears the debug renderer each frame.
            _oculusScreen.DebugRenderer.Clear();

            // A second camera for player B.
            var totalTime = (float)gameTime.TotalGameTime.TotalSeconds;
            var position = Matrix33F.CreateRotationY(totalTime * 0.1f) * new Vector3F(4, 2, 4);
            _cameraNodeB.View = Matrix44F.CreateLookAt(position, new Vector3F(0, 0, 0), new Vector3F(0, 1, 0));
        }
    }

}
