#include<iostream>
#include<conio.h>
#include "edk.h"
#include "edkErrorCode.h"
#include "EmoStateDLL.h"
#include <list>

#pragma comment(lib, "../lib/edk.lib")

using namespace std;

// this sample ( example7) demonstrates how to connect to two headsets at the same time
int main(int argc,char** argv[])
{
	EmoEngineEventHandle hEvent = EE_EmoEngineEventCreate();
	EmoStateHandle eState = EE_EmoStateCreate();
	unsigned int userID = -1;
	list<int> listUser;

	if( EE_EngineConnect() == EDK_OK )
	{
		while(!_kbhit()) 
		{
			int state = EE_EngineGetNextEvent(hEvent);
			if( state == EDK_OK )
			{
				EE_Event_t eventType = EE_EmoEngineEventGetType(hEvent);				
				EE_EmoEngineEventGetUserId(hEvent, &userID);
				if(userID==-1)
					continue;			

				if(eventType == EE_EmoStateUpdated  )
				{								
					// Copies an EmoState returned with a EE_EmoStateUpdate event to memory referenced by an EmoStateHandle. 
					if(EE_EmoEngineEventGetEmoState(hEvent,eState)==EDK_OK)
					{
						if(EE_GetUserProfile(userID,hEvent)==EDK_OK)
						{
							//Affective score, short term excitement						
							cout <<"userID: " << userID  <<endl;
							cout <<"    affectiv excitement score: " << ES_AffectivGetExcitementShortTermScore (eState) << endl;
							cout <<"    expressiv smile extent : " << ES_ExpressivGetSmileExtent(eState) <<endl;							
						}

						
					}					
					
				}
				// userremoved event
				else if( eventType == EE_UserRemoved )
				{
					cout <<"user ID: "<<userID<<" have removed" << endl;	
					listUser.remove(userID);
				}
				// useradded event 
				else if(eventType == EE_UserAdded)
				{
					listUser.push_back(userID);
					cout <<"user ID: "<<userID<<" have added" << endl;
				}		
				userID=-1;
			}			
		}
	}

	EE_EngineDisconnect();
	EE_EmoStateFree(eState);
	EE_EmoEngineEventFree(hEvent);	
	return 0;
}